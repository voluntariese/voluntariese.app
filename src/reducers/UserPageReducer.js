import ActionTypes from '../actions/ActionTypes';

const initialState = {
  orders: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.LIST_USER_ORDERS_SUCCESS:
      return { ...state, orders: action.payload };
    default:
      return state;
  }
};
