import ActionTypes from '../actions/ActionTypes';

const initialState = {
  donationDetail: {
    instituicao: {
      causasInteresse: [{}],
      endereco: {}
    }
  }
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.FILL_DONATION_DETAIL:
      return { ...state, donationDetail: action.payload };
    default:
      return state;
  }
};
