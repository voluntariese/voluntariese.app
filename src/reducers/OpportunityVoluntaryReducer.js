import ActionTypes from '../actions/ActionTypes';

const initialState = {
  opportunities: [],
  opportunityDetail: {
    instituicao: {
      causasInteresse: [{}],
      endereco: {}
    },
    interessados: []
  }
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.SEARCH_VOLUNTARY_OPPORTUNITIES_SUCCESS:
      return { ...state, opportunities: action.payload };
    case ActionTypes.FILL_OPPORTUNITY_VOLUNTARY_DETAIL:
      return { ...state, opportunityDetail: action.payload };
    default:
      return state;
  }
};
