import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import history from '../routes/history';
import LoaderReducer from './LoaderReducer';
import AuthReducer from './AuthReducer';
import PasswordRecoveryReducer from './PasswordRecoveryReducer';
import UserRegisterReducer from './UserRegisterReducer';
import ContactReducer from './ContactReducer';
import UserPageReducer from './UserPageReducer';
import OpportunityListReducer from './OpportunityListReducer';
import DonationListReducer from './DonationListReducer';
import VoluntaryListReducer from './VoluntaryListReducer';
import CauseReducer from './CauseReducer';
import VoluntaryDetailReducer from './VoluntaryDetailReducer';
import DonationDetailReducer from './DonationDetailReducer';
import OpportunityDetailReducer from './OpportunityDetailReducer';
import DonationManagementReducer from './DonationManagementReducer';
import OpportunityManagementReducer from './OpportunityManagementReducer';
import OpportunityVoluntaryReducer from './OpportunityVoluntaryReducer';
import RegisterOpportunityReducer from './RegisterOpportunityReducer';

export default combineReducers({
  router: connectRouter(history),
  loader: LoaderReducer,
  auth: AuthReducer,
  passwordRecovery: PasswordRecoveryReducer,
  userRegister: UserRegisterReducer,
  contact: ContactReducer,
  userPage: UserPageReducer,
  opportunityListReducer: OpportunityListReducer,
  donationListReducer: DonationListReducer,
  voluntaryListReducer: VoluntaryListReducer,
  causeReducer: CauseReducer,
  voluntaryDetailReducer: VoluntaryDetailReducer,
  donationDetailReducer: DonationDetailReducer,
  opportunityDetailReducer: OpportunityDetailReducer,
  donationManagementReducer: DonationManagementReducer,
  opportunityManagementReducer: OpportunityManagementReducer,
  opportunityVoluntaryReducer: OpportunityVoluntaryReducer,
  registerOpportunityReducer: RegisterOpportunityReducer
});
