import ActionTypes from '../actions/ActionTypes';

const initialState = {
  opportunities: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.SEARCH_OPPORTUNITIES_SUCCESS:
      return { ...state, opportunities: action.payload };
    default:
      return state;
  }
};
