import moment from 'moment';
import ActionTypes from '../actions/ActionTypes';

const initialState = {
  causes: [],

  id: null,
  name: '',
  cep: '',
  email: '',
  address: '',
  phone: '',
  city: '',
  state: '',
  password: '',
  confirmation: '',
  cpf: '',
  rg: '',
  number: '',
  complement: '',
  birthday: '',
  genre: '',
  uploadedFile: null,
  type: '',
  neighborhood: '',
  description: '',
  causeOfInterest: 0
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.RESET_REGISTER_FORM:
      return { ...initialState };
    case ActionTypes.UPDATE_USER_REGISTER_FIELD:
      return { ...state, [action.payload.prop]: action.payload.value };
    case ActionTypes.UPLOAD_USER_PICTURE:
      return { ...state, uploadedFile: null };
    case ActionTypes.FILL_CAUSES_SUCCESS:
      return { ...state, causes: action.payload };
    case ActionTypes.UPLOAD_USER_PICTURE_SUCCESS:
      return { ...state, uploadedFile: action.payload };
    case ActionTypes.FILL_USER_REGISTER_FROM_LOGGED_USER:
      return {
        ...initialState,
        id: action.payload.id,
        name: action.payload.nome,
        description: action.payload.descricao,
        type: action.payload.perfil.codigo,
        email: action.payload.email,
        phone: action.payload.telefone,
        genre: action.payload.sexo,
        address: action.payload.endereco.logradouro,
        cep: action.payload.endereco.cep,
        city: action.payload.endereco.cidade,
        number: action.payload.endereco.numero,
        complement: action.payload.endereco.complemento || '',
        state: action.payload.endereco.estado,
        neighborhood: action.payload.endereco.bairro,
        uploadedFile: action.payload.idFotoPerfil
          ? { id: action.payload.idFotoPerfil }
          : null,
        birthday: action.payload.dataNascimento
          ? moment(action.payload.dataNascimento).format('DD/MM/YYYY')
          : null,
        causeOfInterest: action.payload.causasInteresse[0].id
      };
    default:
      return state;
  }
};
