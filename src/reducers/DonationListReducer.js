import ActionTypes from '../actions/ActionTypes';

const initialState = {
  donations: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.SEARCH_DONATIONS_SUCCESS:
      return { ...state, donations: action.payload };
    default:
      return state;
  }
};
