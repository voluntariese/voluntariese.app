import ActionTypes from '../actions/ActionTypes';

const initialState = {
  voluntaryDetail: {
    causasInteresse: [{}],
    endereco: {}
  }
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.FILL_VOLUNTARY_DETAIL:
      return { ...state, voluntaryDetail: action.payload };
    default:
      return state;
  }
};
