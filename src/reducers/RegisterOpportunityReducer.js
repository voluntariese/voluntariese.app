import ActionTypes from '../actions/ActionTypes';

const initialState = {
  causes: [],
  opportunity: {
    turno: '',
    descricao: '',
    qualificacoes: '',
    quantidadeVagas: '',
    causa: {}
  }
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.UPDATE_OPPORTUNITY_REGISTER_FIELD:
      return {
        ...state,
        opportunity: {
          ...state.opportunity,
          [action.payload.prop]: action.payload.value
        }
      };
    case ActionTypes.FILL_CAUSES_OPPORTUNITY_SUCCESS:
      return { ...state, causes: action.payload };
    case ActionTypes.SAVE_OPPORTUNITY_SUCCESS:
    case ActionTypes.UPDATE_OPPORTUNITY_SUCCESS:
    case ActionTypes.FILL_OPPORTUNITY:
      return { ...state, opportunity: action.payload };
    case ActionTypes.CLEAR_OPPORTUNITY:
      return { ...state, opportunity: initialState.opportunity };
    default:
      return state;
  }
};
