import ActionTypes from '../actions/ActionTypes';

const initialState = {
  donations: [],
  donation: {
    descricao: ''
  }
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.SEARCH_MANAGEMENT_DONATIONS_SUCCESS:
      return { ...state, donations: action.payload };
    case ActionTypes.FILL_DONATION_REGISTER:
      return { ...state, donation: action.payload };
    case ActionTypes.SAVE_DONATION_SUCCESS:
      return { ...state, donation: action.payload };
    case ActionTypes.UPDATE_DONATION_REGISTER_FIELD:
      return {
        ...state,
        donation: {
          ...state.donation,
          [action.payload.prop]: action.payload.value
        }
      };
    default:
      return state;
  }
};
