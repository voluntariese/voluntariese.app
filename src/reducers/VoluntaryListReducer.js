import ActionTypes from '../actions/ActionTypes';

const initialState = {
  voluntaries: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.SEARCH_VOLUNTARIES_SUCCESS:
      return { ...state, voluntaries: action.payload };
    default:
      return state;
  }
};
