import ActionTypes from '../actions/ActionTypes';

const initialState = {
  opportunities: [],
  opportunityCandidates: {
    instituicao: {
      causasInteresse: [{}],
      endereco: {}
    },
    interessados: []
  },
  justification: ''
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.SEARCH_MANAGEMENT_OPPORTUNITIES_SUCCESS:
      return { ...state, opportunities: action.payload };
    case ActionTypes.FILL_CANDIDATES:
      return { ...state, opportunityCandidates: action.payload };
    case ActionTypes.UPDATE_JUSTIFICATION_FILED:
      return { ...state, justification: action.payload };
    default:
      return state;
  }
};
