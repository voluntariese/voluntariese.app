import ActionTypes from '../actions/ActionTypes';

const initialState = {
  opportunityDetail: {
    instituicao: {
      causasInteresse: [{}],
      endereco: {}
    }
  }
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.FILL_OPPORTUNITY_DETAIL:
      return { ...state, opportunityDetail: action.payload };
    default:
      return state;
  }
};
