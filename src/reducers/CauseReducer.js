import ActionTypes from '../actions/ActionTypes';

const initialState = {
  causes: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.SEARCH_CAUSES_SUCCESS:
      return { ...state, causes: action.payload };
    default:
      return state;
  }
};
