const passwordValidator = (value, options) => {
  const regex = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[\\+$@$!%*#?&;=,\\._-])[A-Za-z\d\\+$@$!%*#?&;=,\\._-]{6,}$/;
  if (!value) return null;
  return !regex.test(value)
    ? options.message
    : null;
};

export default passwordValidator;
