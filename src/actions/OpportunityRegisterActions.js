import ActionTypes from './ActionTypes';
import Alert from 'react-s-alert';
import {
  NotificationService,
  OpportunityService,
  CauseService
} from '../services';
import { Messages } from '../constants';

export const updateOpportunityRegisterField = ({ prop, value }) => ({
  type: ActionTypes.UPDATE_OPPORTUNITY_REGISTER_FIELD,
  payload: { prop, value }
});

export const fillCauses = () => async dispatch => {
  try {
    dispatch({ type: ActionTypes.FILL_CAUSES_OPPORTUNITY });
    const causes = await CauseService.search();
    dispatch({
      type: ActionTypes.FILL_CAUSES_OPPORTUNITY_SUCCESS,
      payload: causes
    });
  } catch (e) {
    dispatch({ type: ActionTypes.FILL_CAUSES_OPPORTUNITY_FAIL });
    NotificationService.showApiResponseErrorAlert(e);
  }
};

export const registerOpportunity = opportunity => async dispatch => {
  try {
    dispatch({ type: ActionTypes.SAVE_OPPORTUNITY });
    const savedOpportunity = await OpportunityService.save(opportunity);
    dispatch({
      type: ActionTypes.SAVE_OPPORTUNITY_SUCCESS,
      payload: { ...savedOpportunity, causa: savedOpportunity.causa.id }
    });
    Alert.success(Messages.OpportunitySuccessRegisteredMessage);
  } catch (e) {
    NotificationService.showApiResponseErrorAlert(e);
    dispatch({ type: ActionTypes.SAVE_OPPORTUNITY_FAIL });
  }
};

export const updateOpportunity = opportunity => async dispatch => {
  try {
    dispatch({ type: ActionTypes.UPDATE_OPPORTUNITY });
    const savedOpportunity = await OpportunityService.update(opportunity);
    dispatch({
      type: ActionTypes.UPDATE_OPPORTUNITY_SUCCESS,
      payload: { ...savedOpportunity, causa: savedOpportunity.causa.id }
    });
    Alert.success(Messages.OpportunitySuccessUpdatedMessage);
  } catch (e) {
    NotificationService.showApiResponseErrorAlert(e);
    dispatch({ type: ActionTypes.UPDATE_OPPORTUNITY_FAIL });
  }
};
