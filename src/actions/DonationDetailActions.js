import ActionTypes from './ActionTypes';
import { push } from 'connected-react-router';

export const redirectToDonationDetails = donation => async dispatch => {
  dispatch({ type: ActionTypes.FILL_DONATION_DETAIL, payload: donation });
  dispatch(push('/doacoes/detalhes'));
};

export const redirectDonationList = () => async dispatch => {
  dispatch(push('/doacoes'));
};
