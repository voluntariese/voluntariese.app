import ActionTypes from './ActionTypes';
import { NotificationService, CauseService } from '../services';

export const searchCauses = () => async dispatch => {
  try {
    dispatch({ type: ActionTypes.SEARCH_CAUSES });
    const causes = await CauseService.search();
    dispatch({
      type: ActionTypes.SEARCH_CAUSES_SUCCESS,
      payload: causes
    });
  } catch (e) {
    NotificationService.showApiResponseErrorAlert(e);
    dispatch({ type: ActionTypes.SEARCH_CAUSES_FAIL });
  }
};
