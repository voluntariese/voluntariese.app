import { push } from 'connected-react-router';
import Alert from 'react-s-alert';
import ActionTypes from './ActionTypes';
import {
  NotificationService,
  UserService,
  AuthenticationService,
  CauseService
} from '../services';
import { Messages } from '../constants';

export const updateUserRegisterField = ({ prop, value }) => ({
  type: ActionTypes.UPDATE_USER_REGISTER_FIELD,
  payload: { prop, value }
});

export const redirectToProfileEdit = loggedUser => dispatch => {
  fillUserRegisterFromLoggedUser(loggedUser)(dispatch);
  dispatch(push('/meus-dados'));
};

export const redirectToOpportunities = loggedUser => dispatch => {
  dispatch(push('/oportunidades/gerenciador/lista'));
};

export const redirectToMyOpportunities = loggedUser => dispatch => {
  dispatch(push('/oportunidades/minhas-oportunidades'));
};

export const redirectToDonation = loggedUser => dispatch => {
  dispatch(push('/doacoes/gerenciador/lista'));
};

export const fillUserRegisterFromLoggedUser = loggedUser => dispatch =>
  dispatch({
    type: ActionTypes.FILL_USER_REGISTER_FROM_LOGGED_USER,
    payload: loggedUser
  });

export const registerUser = userData => async dispatch => {
  try {
    dispatch({ type: ActionTypes.REGISTER_USER });
    const params = UserService.prepareUserDataToSave(userData);
    await UserService.create(params);
    const user = await AuthenticationService.login(
      userData.email,
      userData.password
    );
    dispatch({
      type: ActionTypes.LOGIN_USER_SUCCESS,
      payload: user
    });
    dispatch({ type: ActionTypes.REGISTER_USER_SUCCESS });
    Alert.success(Messages.UserSuccessRegisteredMessage);
    dispatch(push(''));
  } catch (e) {
    dispatch({ type: ActionTypes.REGISTER_USER_FAIL });
    NotificationService.showApiResponseErrorAlert(e);
  }
};

export const fillCauses = () => async dispatch => {
  try {
    dispatch({ type: ActionTypes.FILL_CAUSES });
    const causes = await CauseService.search();
    dispatch({
      type: ActionTypes.FILL_CAUSES_SUCCESS,
      payload: causes
    });
  } catch (e) {
    dispatch({ type: ActionTypes.FILL_CAUSES_FAIL });
    NotificationService.showApiResponseErrorAlert(e);
  }
};

export const uploadProfilePicture = file => async dispatch => {
  try {
    dispatch({ type: ActionTypes.UPLOAD_USER_PICTURE });
    const uploadedFile = file
      ? await UserService.uploadProfilePicture(file)
      : null;
    dispatch({
      type: ActionTypes.UPLOAD_USER_PICTURE_SUCCESS,
      payload: uploadedFile
    });
  } catch (e) {
    dispatch({ type: ActionTypes.UPLOAD_USER_PICTURE_FAIL });
  }
};

export const resetUserRegisterForm = () => ({
  type: ActionTypes.RESET_REGISTER_FORM
});
