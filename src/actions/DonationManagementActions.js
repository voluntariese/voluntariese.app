import ActionTypes from './ActionTypes';
import Alert from 'react-s-alert';
import { NotificationService, DonationService } from '../services';
import { push } from 'connected-react-router';
import { Messages } from '../constants';

export const searchManagementDonations = () => async dispatch => {
  try {
    dispatch({ type: ActionTypes.SEARCH_MANAGEMENT_DONATIONS });
    const donations = await DonationService.searchByUser();
    dispatch({
      type: ActionTypes.SEARCH_MANAGEMENT_DONATIONS_SUCCESS,
      payload: donations
    });
  } catch (e) {
    NotificationService.showApiResponseErrorAlert(e);
    dispatch({ type: ActionTypes.SEARCH_MANAGEMENT_DONATIONS_FAIL });
  }
};

export const redirectToDonationEdit = donation => async dispatch => {
  dispatch({ type: ActionTypes.FILL_DONATION_REGISTER, payload: donation });
  dispatch(push('/doacoes/gerenciador/cadastro'));
};

export const redirectToAddDonation = () => async dispatch => {
  dispatch({
    type: ActionTypes.FILL_DONATION_REGISTER,
    payload: { descricao: '' }
  });
  dispatch(push('/doacoes/gerenciador/cadastro'));
};

export const updateDonationRegisterField = ({ prop, value }) => ({
  type: ActionTypes.UPDATE_DONATION_REGISTER_FIELD,
  payload: { prop, value }
});

export const registerDonation = donation => async dispatch => {
  try {
    dispatch({ type: ActionTypes.SAVE_DONATION });
    const savedDonation = await DonationService.save(donation);
    dispatch({
      type: ActionTypes.SAVE_DONATION_SUCCESS,
      payload: savedDonation
    });
    Alert.success(Messages.DonationSuccessRegisteredMessage);
  } catch (e) {
    NotificationService.showApiResponseErrorAlert(e);
    dispatch({ type: ActionTypes.SAVE_DONATION_FAIL });
  }
};

export const updateDonation = donation => async dispatch => {
  try {
    dispatch({ type: ActionTypes.UPDATE_DONATION });
    const savedDonation = await DonationService.update(donation);
    dispatch({
      type: ActionTypes.UPDATE_DONATION_SUCCESS,
      payload: savedDonation
    });
    Alert.success(Messages.DonationSuccessUpdatedMessage);
  } catch (e) {
    NotificationService.showApiResponseErrorAlert(e);
    dispatch({ type: ActionTypes.UPDATE_DONATION_FAIL });
  }
};
