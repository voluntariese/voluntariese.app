import ActionTypes from './ActionTypes';
import { NotificationService, DonationService } from '../services';

export const searchDonations = (idCausa = null) => async dispatch => {
  try {
    dispatch({ type: ActionTypes.SEARCH_DONATIONS });
    const donations = await DonationService.search(idCausa);
    dispatch({
      type: ActionTypes.SEARCH_DONATIONS_SUCCESS,
      payload: donations
    });
  } catch (e) {
    NotificationService.showApiResponseErrorAlert(e);
    dispatch({ type: ActionTypes.SEARCH_DONATIONS_FAIL });
  }
};
