import ActionTypes from './ActionTypes';
import { NotificationService, OpportunityService } from '../services';
import { push } from 'connected-react-router';

export const searchVoluntaryOpportunities = () => async dispatch => {
  try {
    dispatch({ type: ActionTypes.SEARCH_VOLUNTARY_OPPORTUNITIES });
    const opportunities = await OpportunityService.searchByVoluntary();
    dispatch({
      type: ActionTypes.SEARCH_VOLUNTARY_OPPORTUNITIES_SUCCESS,
      payload: opportunities
    });
  } catch (e) {
    NotificationService.showApiResponseErrorAlert(e);
    dispatch({ type: ActionTypes.SEARCH_VOLUNTARY_OPPORTUNITIES_FAIL });
  }
};

export const redirectToOpportunityDetail = opportunity => async dispatch => {
  dispatch({
    type: ActionTypes.FILL_OPPORTUNITY_VOLUNTARY_DETAIL,
    payload: opportunity
  });
  dispatch(push('/oportunidades/minhas-oportunidades/detalhes'));
};

export const redirectToVoluntaryList = () => async dispatch => {
  dispatch(push('/oportunidades/minhas-oportunidades'));
};
