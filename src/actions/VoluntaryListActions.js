import ActionTypes from './ActionTypes';
import { NotificationService, VoluntaryService } from '../services';

export const searchVoluntaries = (idCausa = null) => async dispatch => {
  try {
    dispatch({ type: ActionTypes.SEARCH_VOLUNTARIES });
    const voluntaries = await VoluntaryService.search(idCausa);
    dispatch({
      type: ActionTypes.SEARCH_VOLUNTARIES_SUCCESS,
      payload: voluntaries
    });
  } catch (e) {
    NotificationService.showApiResponseErrorAlert(e);
    dispatch({ type: ActionTypes.SEARCH_VOLUNTARIES_FAIL });
  }
};
