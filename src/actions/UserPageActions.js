import { push } from 'connected-react-router';
import Alert from 'react-s-alert';
import ActionTypes from './ActionTypes';
import { NotificationService, UserService } from '../services';
import { Messages } from '../constants';

export const fillUserRegisterFromLoggedUser = (loggedUser) => dispatch => 
  dispatch({ type: ActionTypes.FILL_USER_REGISTER_FROM_LOGGED_USER, payload: loggedUser });

export const updateUserProfile = (userData) => async dispatch => {
  try {
    dispatch({ type: ActionTypes.UPDATE_USER_PROFILE });
    const params = UserService.prepareUserDataToSave(userData);
    const user = await UserService.update(params);
    dispatch({ type: ActionTypes.UPDATE_USER_PROFILE_SUCCESS, payload: user });
    Alert.success(Messages.UserProfileUpdatedSuccessMessage);
  } catch (e) {
    NotificationService.showApiResponseErrorAlert(e);
    dispatch({ type: ActionTypes.UPDATE_USER_PROFILE_FAIL });
  }
};

