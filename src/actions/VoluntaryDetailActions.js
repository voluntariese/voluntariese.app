import ActionTypes from './ActionTypes';
import { push } from 'connected-react-router';

export const redirectToVoluntaryDetails = voluntary => async dispatch => {
  dispatch({ type: ActionTypes.FILL_VOLUNTARY_DETAIL, payload: voluntary });
  dispatch(push('/voluntarios/detalhes'));
};

export const redirectVoluntaryList = () => async dispatch => {
  dispatch(push('/voluntarios'));
};
