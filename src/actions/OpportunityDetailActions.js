import ActionTypes from './ActionTypes';
import { OpportunityService, NotificationService } from '../services';
import Alert from 'react-s-alert';
import { Messages } from '../constants';
import { push } from 'connected-react-router';

export const redirectToOpportunityDetails = opportunity => async dispatch => {
  dispatch({ type: ActionTypes.FILL_OPPORTUNITY_DETAIL, payload: opportunity });
  dispatch(push('/oportunidades/detalhes'));
};

export const redirectOpportunityList = () => async dispatch => {
  dispatch(push('/oportunidades'));
};

export const candidated = opportunity => async dispatch => {
  try {
    dispatch({ type: ActionTypes.CANDIDATED_TO_OPPORTUNITY });
    await OpportunityService.candidated(opportunity);
    dispatch({
      type: ActionTypes.CANDIDATED_TO_OPPORTUNITY_SUCCESS
    });
    Alert.success(Messages.VoluntarySuccessCandidatedMessage);
  } catch (e) {
    NotificationService.showApiResponseErrorAlert(e);
    dispatch({ type: ActionTypes.CANDIDATED_TO_OPPORTUNITY_FAIL });
  }
};
