import ActionTypes from './ActionTypes';
import { NotificationService, OpportunityService } from '../services';

export const searchOpportunities = (id = null) => async dispatch => {
  try {
    dispatch({ type: ActionTypes.SEARCH_OPPORTUNITIES });
    const opportunities = await OpportunityService.search(id);
    dispatch({
      type: ActionTypes.SEARCH_OPPORTUNITIES_SUCCESS,
      payload: opportunities
    });
  } catch (e) {
    NotificationService.showApiResponseErrorAlert(e);
    dispatch({ type: ActionTypes.SEARCH_OPPORTUNITIES_FAIL });
  }
};
