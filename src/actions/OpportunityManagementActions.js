import ActionTypes from './ActionTypes';
import { NotificationService, OpportunityService } from '../services';
import { push } from 'connected-react-router';
import Alert from 'react-s-alert';
import { Messages } from '../constants';

export const searchManagementOpportunities = () => async dispatch => {
  try {
    dispatch({ type: ActionTypes.SEARCH_MANAGEMENT_OPPORTUNITIES });
    const opportunities = await OpportunityService.searchByUser();
    dispatch({
      type: ActionTypes.SEARCH_MANAGEMENT_OPPORTUNITIES_SUCCESS,
      payload: opportunities
    });
  } catch (e) {
    NotificationService.showApiResponseErrorAlert(e);
    dispatch({ type: ActionTypes.SEARCH_MANAGEMENT_OPPORTUNITIES_FAIL });
  }
};

export const redirectToEditOpportunity = opportunity => async dispatch => {
  dispatch({
    type: ActionTypes.FILL_OPPORTUNITY,
    payload: { ...opportunity, causa: opportunity.causa.id }
  });
  dispatch(push('/oportunidades/gerenciador/cadastro'));
};

export const redirectToAddOpportunity = () => async dispatch => {
  dispatch({
    type: ActionTypes.CLEAR_OPPORTUNITY
  });
  dispatch(push('/oportunidades/gerenciador/cadastro'));
};

export const redirectOpportunityList = () => async dispatch => {
  dispatch(push('/oportunidades/gerenciador/lista'));
};

export const redirectToCandidates = opportunity => async dispatch => {
  dispatch({
    type: ActionTypes.FILL_CANDIDATES,
    payload: opportunity
  });
  dispatch(push('/oportunidades/gerenciador/candidatos'));
};

export const approve = opportunity => async dispatch => {
  try {
    dispatch({ type: ActionTypes.APPROVE_OPPORTUNITY });
    await OpportunityService.approve(opportunity);
    dispatch({ type: ActionTypes.APPROVE_OPPORTUNITY_SUCCESS });
    Alert.success(Messages.ApproveOpportunitySuccessdMessage);
    dispatch(push('/oportunidades/gerenciador/lista'));
  } catch (e) {
    NotificationService.showApiResponseErrorAlert(e);
    dispatch({ type: ActionTypes.APPROVE_OPPORTUNITY_FAIL });
  }
};

export const reproveOpportunity = (
  opportunity,
  justification
) => async dispatch => {
  try {
    dispatch({ type: ActionTypes.REPROVE_OPPORTUNITY });
    await OpportunityService.reprove(opportunity, justification);
    dispatch({ type: ActionTypes.REPROVE_OPPORTUNITY_SUCCESS });
    Alert.success(Messages.ReprovedOpportunitySuccessdMessage);
    dispatch(push('/oportunidades/gerenciador/lista'));
  } catch (e) {
    NotificationService.showApiResponseErrorAlert(e);
    dispatch({ type: ActionTypes.REPROVE_OPPORTUNITY_FAIL });
  }
};

export const updateJustification = justification => async dispatch => {
  dispatch({
    type: ActionTypes.UPDATE_JUSTIFICATION_FILED,
    payload: justification
  });
};
