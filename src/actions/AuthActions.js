import { push } from 'connected-react-router';
import Alert from 'react-s-alert';
import ActionTypes from './ActionTypes';
import { AuthenticationService, NotificationService } from '../services';
import { Messages } from '../constants';

export const login = (email, password) => async dispatch => {
  try {
    dispatch({ type: ActionTypes.LOGIN_USER });
    const user = await AuthenticationService.login(email, password);
    dispatch({
      type: ActionTypes.LOGIN_USER_SUCCESS,
      payload: user
    });
    dispatch(push('/'))
  } catch (e) {
    dispatch({ type: ActionTypes.LOGIN_USER_FAIL });
    NotificationService.showApiResponseErrorAlert(e);
  }
};

export const requestPasswordRecovery = (email) => async dispatch => {
  try {
    dispatch({ type: ActionTypes.REQUEST_PASSWORD_RECOVERY });
    await AuthenticationService.sendRecoveryPasswordEmail(email);
    dispatch({ type: ActionTypes.REQUEST_PASSWORD_RECOVERY_SUCCESS });
    Alert.success(Messages.PasswordRecoveryEmailSentMessage);
    dispatch(push('esqueci-minha-senha/codigo'))
  } catch (e) {
    dispatch({ type: ActionTypes.REQUEST_PASSWORD_RECOVERY_FAIL });
    NotificationService.showApiResponseErrorAlert(e);
  }
};

export const validatePasswordRecoveryCode = (code) => async dispatch => {
  try {
    dispatch({ type: ActionTypes.VALIDATE_PASSWORD_RECOVERY_CODE });
    await AuthenticationService.validateRecoveryPasswordCode(code);
    dispatch({ type: ActionTypes.VALIDATE_PASSWORD_RECOVERY_CODE_SUCCESS });
    Alert.success(Messages.PasswordRecoveryCodeSuccessValidatedMessage);
    dispatch(push(`/esqueci-minha-senha/codigo/${code}`));
  } catch (e) {
    dispatch({ type: ActionTypes.VALIDATE_PASSWORD_RECOVERY_CODE_FAIL });
    NotificationService.showApiResponseErrorAlert(e);
  }
};

export const recoverPassword = (code, password, confirmation) => async dispatch => {
  try {
    dispatch({ type: ActionTypes.RECOVER_PASSWORD });
    await AuthenticationService.recoverPassword(code, password, confirmation);
    dispatch({ type: ActionTypes.RECOVER_PASSWORD_SUCCESS });
    Alert.success(Messages.PasswordSuccessRecoveredMessage);
    dispatch(push('/login'));
  } catch (e) {
    dispatch({ type: ActionTypes.RECOVER_PASSWORD_FAIL });
    NotificationService.showApiResponseErrorAlert(e);
  }
};

export const logout = () => dispatch => {
  AuthenticationService.logout();
  dispatch({ type: ActionTypes.LOGOUT_USER_SUCCESS });
  dispatch(push('/'));
};

export const verifyIfUserIsLogged = () => (dispatch) => {
  const email = AuthenticationService.getAuthEmail();
  updateAuthEmail(email)(dispatch);
  const user = AuthenticationService.userIsLogged() 
    ? AuthenticationService.getLoggedUser() 
    : null;
  dispatch({ 
    type: ActionTypes.VERIFY_LOGGED_USER_SUCCESS,
    payload: user
  });
};

export const updateAuthEmail = (email) => (dispatch) => {
  dispatch({
    type: ActionTypes.UPDATE_AUTH_EMAIL,
    payload: email || ''
  });
};

export const updateLoginField = ({ prop, value }) => (
  {
    type: ActionTypes.UPDATE_LOGIN_FIELD,
    payload: { prop, value }
  }
);

export const updatePasswordRecoveryField = ({ prop, value }) => (
  {
    type: ActionTypes.UPDATE_PASSWORD_RECOVERY_FIELD,
    payload: { prop, value }
  }
);