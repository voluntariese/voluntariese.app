import React, { Component } from 'react';
import { VoluntaryBoxComponent } from '..';
import './style.scss';

class VoluntaryListComponent extends Component {
  render = () => {
    const { voluntaries, colLg, colMd, colSm, colXs } = this.props;
    return (
      <div className="voluntary-list row">
        {voluntaries.map(v => (
          <div
            key={v.id}
            className={`${colLg ? colLg : 'col-lg-4'} ${
              colMd ? colMd : 'col-md-4'
            } ${colSm ? colSm : 'col-sm-6'} ${colXs ? colXs : 'col-xs-12'}`}
            key={v.id}
          >
            <VoluntaryBoxComponent voluntary={v} />
          </div>
        ))}
        {voluntaries.length === 0 && (
          <p className="text text-center">Nenhum voluntário encontrado</p>
        )}
      </div>
    );
  };
}

export default VoluntaryListComponent;
