import React, { Component } from 'react';
import Alert from 'react-s-alert';
import { Provider } from 'react-redux';
import ReactGA from 'react-ga';
import { registerCustomValidators } from '../../validators';
import Routes from '../../routes';
import store from '../../store';
import history from '../../routes/history';
import '../../shared/common.scss';
import 'bootstrap/dist/css/bootstrap.css';
import 'jquery/src/jquery';
import 'bootstrap/dist/js/bootstrap.min';
import 'react-s-alert/dist/s-alert-default.css';
import 'react-s-alert/dist/s-alert-css-effects/flip.css';

const DEFAULT_CONFIG = {
  trackingId: process.env.REACT_APP_ANALYTICS_CODE,
  debug: true,
  gaOptions: {
    cookieDomain: 'none'
  }
};

class App extends Component {
  unlisten = null;

  alertConfig = {
    position: 'top-right',
    effect: 'flip'
  };

  componentDidMount = () => {
    registerCustomValidators();
    ReactGA.initialize(DEFAULT_CONFIG);
  };

  componentWillMount = () => {
    this.unlisten = history.listen(location => {
      ReactGA.pageview(location.pathname + location.search);
    });
  };

  componentWillUnmount = () => {
    this.unlisten();
  };

  render() {
    return (
      <Provider store={store}>
        <Routes />
        <Alert
          stack={{ limit: 5 }}
          html
          position={'top-right'}
          effect={'flip'}
          timeout={5000}
        />
      </Provider>
    );
  }
}

export default App;
