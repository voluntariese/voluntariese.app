import React, { Component } from 'react';
import Slider from 'react-slick';
import './style.scss';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

class Banner extends Component { 

  settings = {
    dots: false,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1
  };

  render = () => (
    <div className="banner hidden-xs">
      <Slider {...this.settings}>
        <img src="/img/banners/banner4.jpg" />
        <img src="/img/banners/banner3.jpg" />
        <img src="/img/banners/banner1.jpg" />
        <img src="/img/banners/banner2.jpg" />
      </Slider>
    </div>
  );
}

export default Banner;