import React, { Component } from 'react';
import * as OpportunityDetailActions from '../../actions/OpportunityDetailActions';
import { connect } from 'react-redux';
import './style.scss';

class OpportunityBox extends Component {
  redirectToOpportunityDetail = opportunity => {
    this.props.redirectToOpportunityDetails(opportunity);
  };

  render = () => {
    const { opportunity } = this.props;
    return (
      <div className="opportunity-box">
        <div className="text-center img-box">
          <div onClick={() => this.redirectToOpportunityDetail(opportunity)}>
            {opportunity.instituicao.idFotoPerfil && (
              <img
                src={`${process.env.REACT_APP_API_URL}/arquivos/${
                  opportunity.instituicao.idFotoPerfil
                }`}
                className="img-inst"
              />
            )}
            {!opportunity.instituicao.idFotoPerfil && (
              <i className="fas fa-hand-holding-heart opportunity-image" />
            )}
          </div>
        </div>
        <div onClick={() => this.redirectToOpportunityDetail(opportunity)}>
          <h4 className="title text-center">{opportunity.instituicao.nome}</h4>
          <p className="text small gray text-center indication">
            {opportunity.descricao}
          </p>
        </div>
        <div className="options">
          <p className="text gray text-center bold price padding">
            <i className="fas fa-heart icon-option" />
            Quantidade de vagas: {opportunity.quantidadeVagas}
          </p>
          <p className="text gray text-center bold price">
            <i className="fas fa-map-marker-alt icon-option" />
            {` ${opportunity.instituicao.endereco.cidade} / ${
              opportunity.instituicao.endereco.estado
            }`}
          </p>
          <div className="btn-container text-center">
            <div
              onClick={() => this.redirectToOpportunityDetail(opportunity)}
              className="btn detail"
            >
              <img
                src="/img/icons/eye.png"
                srcSet="/img/icons/eye@2x.png 2x, /img/icons/eye@3x.png 3x"
              />
            </div>
          </div>
        </div>
      </div>
    );
  };
}

const mapStateToProps = ({ opportunityDetailReducer }) => {
  return {
    ...opportunityDetailReducer
  };
};

export default connect(
  mapStateToProps,
  { ...OpportunityDetailActions }
)(OpportunityBox);
