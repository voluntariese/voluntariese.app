import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as CauseActions from '../../actions/CauseActions';
import * as VoluntaryListActions from '../../actions/VoluntaryListActions';
import './style.scss';

class VoluntaryMenu extends Component {
  state = {
    selectedCategory: null
  };

  componentDidMount = () => {
    this.props.searchCauses();
  };

  filterItens = id => {
    this.props.searchVoluntaries(id);
    this.setState({ selectedCategory: id });
  };

  clearFilter = () => {
    this.props.searchVoluntaries();
    this.setState({ selectedCategory: null });
  };

  render = () => {
    return (
      <ul className="list-unstyled categories">
        <li>
          <div onClick={e => this.clearFilter()} className="pointer">
            <h3 className="title primary">Causas</h3>
          </div>
          <ul className="list-unstyled" id="categories">
            {this.props.causes.map(c => (
              <li
                key={c.id}
                className={this.state.selectedCategory == c.id ? 'active' : ''}
              >
                <div onClick={e => this.filterItens(c.id)} className="pointer">
                  <img src={`/img/icons/${c.icone}.png`} />
                  <span className="text">{c.descricao}</span>
                </div>
              </li>
            ))}
          </ul>
        </li>
      </ul>
    );
  };
}

const mapStateToProps = ({ causeReducer }) => {
  return {
    ...causeReducer
  };
};

export default connect(
  mapStateToProps,
  { ...CauseActions, ...VoluntaryListActions }
)(VoluntaryMenu);
