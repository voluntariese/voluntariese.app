import React, { Component } from 'react';
import { OpportunityBoxComponent } from '..';
import './style.scss';

class OpportunityListComponent extends Component {
  render = () => {
    const { opportunities, colLg, colMd, colSm, colXs } = this.props;
    return (
      <div className="opportunity-list row">
        {opportunities.map(o => (
          <div
            key={o.id}
            className={`${colLg ? colLg : 'col-lg-4'} ${
              colMd ? colMd : 'col-md-4'
            } ${colSm ? colSm : 'col-sm-6'} ${colXs ? colXs : 'col-xs-12'}`}
            key={o.id}
          >
            <OpportunityBoxComponent opportunity={o} />
          </div>
        ))}
        {opportunities.length === 0 && (
          <p className="text text-center">Nenhuma oportunidade encontrada</p>
        )}
      </div>
    );
  };
}

export default OpportunityListComponent;
