import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as DonationDetailActions from '../../actions/DonationDetailActions';
import './style.scss';

class DonateBoxComponent extends Component {
  redirectToDonateDetail = donate => {
    this.props.redirectToDonationDetails(donate);
  };

  render = () => {
    const { donation } = this.props;
    return (
      <div className="donate-box">
        <div className="text-center img-box">
          <div onClick={() => this.redirectToDonateDetail(donation)}>
            {donation.instituicao.idFotoPerfil && (
              <img
                src={`${process.env.REACT_APP_API_URL}/arquivos/${
                  donation.instituicao.idFotoPerfil
                }`}
                className="img-inst"
              />
            )}
            {!donation.instituicao.idFotoPerfil && (
              <i className="fas fa-university donate-image" />
            )}
          </div>
        </div>
        <div onClick={() => this.redirectToDonateDetail(donation)}>
          <h4 className="title text-center">{donation.instituicao.nome}</h4>
          <p className="text small gray text-center indication">
            {donation.descricao}
          </p>
        </div>
        <div className="options">
          <p className="text gray text-center bold price padding">
            <i className="fas fa-phone icon-option" />
            {donation.instituicao.telefone}
          </p>
          <p className="text gray text-center bold price">
            <i className="fas fa-envelope icon-option" />
            {donation.instituicao.email}
          </p>
          <div className="btn-container text-center">
            <div
              onClick={() => this.redirectToDonateDetail(donation)}
              className="btn detail"
            >
              <img
                src="/img/icons/eye.png"
                srcSet="/img/icons/eye@2x.png 2x, /img/icons/eye@3x.png 3x"
              />
            </div>
          </div>
        </div>
      </div>
    );
  };
}

const mapStateToProps = ({ donationDetailReducer }) => {
  return {
    ...donationDetailReducer
  };
};

export default connect(
  mapStateToProps,
  { ...DonationDetailActions }
)(DonateBoxComponent);
