import React, { Component } from 'react';
import './style.scss';

class Footer extends Component {
  render = () => (
    <footer className="footer">
      <div className="container">
        <div className="row">
          <div className="col-lg-4 col-md-6 col-sm-12 col-xs-12 by">
            <span className="text by-text">By: Gustavo Mello</span>
          </div>
          <div className="col-lg-6 col-md-4 col-sm-6 col-xs-8 by">
          <div>
            <span className="text by-text">Trabalho de conclusão de curso - ADS</span> 
          </div>
          <div>
            <span className="text teacher">Orientadora: Professora Margrit Reni Krug.</span> 
          </div>
          </div>
          <div className="col-lg-2 col-md-2 col-sm-6 col-xs-4">
            <a href="http://www.unisinos.br/" target="_blank">
              <img src="/img/icons/logo_unisinos.png" className="logo"/>
            </a>
          </div>
        </div>
      </div>
    </footer>
  )
}
 
export default Footer;