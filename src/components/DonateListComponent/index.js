import React, { Component } from 'react';
import { DonateBoxComponent } from '..';
import './style.scss';

class DonateListComponent extends Component {
  render = () => {
    const { donations, colLg, colMd, colSm, colXs } = this.props;
    return (
      <div className="donate-list row">
        {donations.map(d => (
          <div
            key={d.id}
            className={`${colLg ? colLg : 'col-lg-4'} ${
              colMd ? colMd : 'col-md-4'
            } ${colSm ? colSm : 'col-sm-6'} ${colXs ? colXs : 'col-xs-12'}`}
            key={d.id}
          >
            <DonateBoxComponent donation={d} />
          </div>
        ))}
        {donations.length === 0 && (
          <p className="text text-center">Nenhuma doação encontrada</p>
        )}
      </div>
    );
  };
}

export default DonateListComponent;
