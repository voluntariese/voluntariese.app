import React, { Component } from 'react';
import * as VoluntaryDetailActions from '../../actions/VoluntaryDetailActions';
import { connect } from 'react-redux';
import './style.scss';

class VoluntaryBoxComponent extends Component {
  redirectToVoluntaryDetail = voluntary => {
    this.props.redirectToVoluntaryDetails(voluntary);
  };

  render = () => {
    const { voluntary } = this.props;
    return (
      <div className="voluntary-box">
        <div className="text-center img-box">
          <div onClick={() => this.redirectToVoluntaryDetail(voluntary)}>
            {voluntary.idFotoPerfil && (
              <img
                src={`${process.env.REACT_APP_API_URL}/arquivos/${
                  voluntary.idFotoPerfil
                }`}
                className="img-inst"
              />
            )}
            {!voluntary.idFotoPerfil && (
              <i className="fas fa-user voluntary-image" />
            )}
          </div>
        </div>
        <div onClick={() => this.redirectToVoluntaryDetail(voluntary)}>
          <h4 className="title text-center">{voluntary.nome}</h4>
          <p className="text small gray text-center indication">
            {voluntary.qualificacoes}
          </p>
        </div>
        <div className="options">
          <p className="text gray text-center bold price padding">
            <i className="fas fa-phone icon-option" />
            {voluntary.telefone}
          </p>
          <p className="text gray text-center bold price">
            <i className="fas fa-envelope icon-option" />
            {voluntary.email}
          </p>
          <div className="btn-container text-center">
            <div
              onClick={() => this.redirectToVoluntaryDetail(voluntary)}
              className="btn detail"
            >
              <img
                src="/img/icons/eye.png"
                srcSet="/img/icons/eye@2x.png 2x, /img/icons/eye@3x.png 3x"
              />
            </div>
          </div>
        </div>
      </div>
    );
  };
}

const mapStateToProps = ({ voluntaryDetailReducer }) => {
  return {
    ...voluntaryDetailReducer
  };
};

export default connect(
  mapStateToProps,
  { ...VoluntaryDetailActions }
)(VoluntaryBoxComponent);
