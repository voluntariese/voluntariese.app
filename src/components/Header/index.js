import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { logout } from '../../actions/AuthActions';
import {
  resetUserRegisterForm,
  redirectToProfileEdit,
  redirectToOpportunities,
  redirectToDonation,
  redirectToMyOpportunities
} from '../../actions/UserRegisterActions';
import './style.scss';
import Loader from '../Loader';
import { UserProfile } from '../../constants';

class Header extends Component {
  state = {
    searchOpened: false,
    searchTerm: ''
  };

  constructor(props) {
    super(props);
    this.logoutUser = this.logoutUser.bind(this);
    this.handleSearchTermChange = this.handleSearchTermChange.bind(this);
    this.handleSearchSubmit = this.handleSearchSubmit.bind(this);
  }

  handleSearchTermChange = event => {
    const { value } = event.target;
    this.setState({ searchTerm: value });
  };

  handleSearchSubmit = event => {
    event.preventDefault();
    this.props.redirectToProductSearch(this.state.searchTerm);
    this.setState({ searchTerm: '' });
  };

  redirectToOpportunities = () => {
    if (this.props.loggedUser.perfil.codigo === UserProfile.Instituicao)
      this.props.redirectToOpportunities(this.props.loggedUser);
    else this.props.redirectToMyOpportunities(this.props.loggedUser);
  };

  redirectToProfileEdit = () => {
    this.props.redirectToProfileEdit(this.props.loggedUser);
  };

  redirectToDonation = () => {
    this.props.redirectToDonation(this.props.loggedUser);
  };

  logoutUser = () => this.props.logout();

  render = () => {
    const { loggedUser } = this.props;
    return (
      <header className="header">
        <Loader />
        <nav className="navbar navbar-default">
          <div className="container">
            <div className="navbar-header">
              <button
                type="button"
                className="navbar-toggle collapsed"
                data-toggle="collapse"
                data-target="#navbar-collapse"
                aria-expanded="false"
              >
                <span className="sr-only">Toggle navigation</span>
                <span className="icon-bar" />
                <span className="icon-bar" />
                <span className="icon-bar" />
              </button>
              <Link className="navbar-brand" to="/">
                <img
                  alt="Logo"
                  src="/img/logo-header.png"
                  width="100px"
                  className="logo"
                />
              </Link>
            </div>

            <div className="collapse navbar-collapse" id="navbar-collapse">
              <ul className="nav navbar-nav left-nav">
                <li>
                  <Link to="/voluntarios">
                    <i className="fas fa-hands-helping menu-icon" />
                    <span className="text">Voluntários</span>
                  </Link>
                </li>
                <li>
                  <Link to="/oportunidades">
                    <i className="fas fa-hand-holding-heart menu-icon" />
                    <span className="text">Oportunidades</span>
                  </Link>
                </li>
                <li>
                  <Link to="/doacoes">
                    <i className="fas fa-box menu-icon" />
                    <span className="text">Doações</span>
                  </Link>
                </li>
                <li>
                  <Link to="/contato">
                    <i className="fas fa-envelope menu-icon" />
                    <span className="text">Contato</span>
                  </Link>
                </li>
              </ul>
              <ul className="nav navbar-nav navbar-right">
                {!loggedUser && (
                  <li
                    className={
                      this.state.searchOpened ? 'hidden visible-xs' : ''
                    }
                  >
                    <Link to="/login">
                      <img
                        src="/img/icons/user.png"
                        srcSet="/img/icons/user@2x.png 2x,/img/icons/user@3x.png 3x"
                        className="icon hidden-xs"
                      />
                      <span className="text">Login</span>
                    </Link>
                  </li>
                )}
                {loggedUser && (
                  <li
                    className={`dropdown ${
                      this.state.searchOpened ? 'hidden visible-xs' : ''
                    }`}
                  >
                    <Link
                      to="/login"
                      className="dropdown-toggle"
                      data-toggle="dropdown"
                      role="button"
                      aria-haspopup="true"
                      aria-expanded="false"
                    >
                      {loggedUser.idFotoPerfil && (
                        <img
                          src={`${process.env.REACT_APP_API_URL}/arquivos/${
                            loggedUser.idFotoPerfil
                          }`}
                          className="icon avatar hidden-xs"
                        />
                      )}
                      {!loggedUser.idFotoPerfil && (
                        <img
                          src="/img/icons/user.png"
                          srcSet="/img/icons/user@2x.png 2x,/img/icons/user@3x.png 3x"
                          className="icon hidden-xs"
                        />
                      )}

                      <span className="text">
                        Olá, {this.props.loggedUser.nome}!{' '}
                        <span className="caret" />
                      </span>
                    </Link>
                    <ul className="dropdown-menu">
                      <li>
                        <a
                          href="javascript:"
                          onClick={this.redirectToProfileEdit}
                        >
                          {loggedUser.perfil.codigo === UserProfile.Instituicao
                            ? 'Gerenciar Perfil'
                            : 'Meus Dados'}
                        </a>
                      </li>
                      <li>
                        <a
                          href="javascript:"
                          onClick={this.redirectToOpportunities}
                        >
                          {loggedUser.perfil.codigo === UserProfile.Instituicao
                            ? 'Gerenciar Oportunidades'
                            : 'Minhas Oportunidades'}
                        </a>
                      </li>
                      {loggedUser.perfil.codigo === UserProfile.Instituicao && (
                        <li>
                          <a
                            href="javascript:"
                            onClick={this.redirectToDonation}
                          >
                            Gerenciar Solicitações Doações
                          </a>
                        </li>
                      )}
                      <li>
                        <a href="javascript:" onClick={this.logoutUser}>
                          Sair
                        </a>
                      </li>
                    </ul>
                  </li>
                )}

                <li
                  className={`basket ${
                    this.state.searchOpened ? 'hidden visible-xs' : ''
                  }`}
                />
              </ul>
            </div>
          </div>
        </nav>
      </header>
    );
  };
}

const mapStateToProps = ({ auth }) => {
  const { loggedUser } = auth;
  return {
    loggedUser
  };
};

export default connect(
  mapStateToProps,
  {
    logout,
    resetUserRegisterForm,
    redirectToProfileEdit,
    redirectToOpportunities,
    redirectToDonation,
    redirectToMyOpportunities
  }
)(Header);
