import { AuthenticationService } from "../services";

export const isAuthenticated = () => {
  const isAuthenticated = AuthenticationService.userIsLogged();
  if (!isAuthenticated) {
    AuthenticationService.logout();
    return false;
  }
  return true;
};