import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { ConnectedRouter } from 'connected-react-router';
import { connect } from 'react-redux';
import { verifyIfUserIsLogged } from '../actions/AuthActions';
import { resetUserRegisterForm } from '../actions/UserRegisterActions';
import { fillUserRegisterFromLoggedUser } from '../actions/UserPageActions';
import history from './history';
import { isAuthenticated } from './auth';
import {
  Contact,
  Login,
  Register,
  ForgotPassword,
  PasswordRecoveryCode,
  PasswordRecovery,
  VoluntaryList,
  OpportunityList,
  DonationList,
  Home,
  Opportunity,
  Donation,
  VoluntaryDetail,
  DonationDetail,
  OpportunityDetail,
  DonationManagement,
  DonationRegister,
  OpportunityManagement,
  OpportunityRegister,
  OpportunityVoluntaryList,
  OpportunityVoluntaryDetail,
  OpportunityCandidates
} from '../screens';
import { AuthenticationService } from '../services';
import { UserProfile } from '../constants';

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props => {
      const userIsAuthenticated = isAuthenticated();
      if (!userIsAuthenticated)
        return (
          <Redirect
            to={{ pathname: '/login', state: { from: props.location } }}
          />
        );

      const loggedUser = AuthenticationService.getLoggedUser();
      if (rest.onlyAdmin && loggedUser.perfil.codigo !== UserProfile.Admin)
        return (
          <Redirect to={{ pathname: '/', state: { from: props.location } }} />
        );

      return <Component {...props} />;
    }}
  />
);

class Routes extends Component {
  unlisten = null;

  state = {
    reduxLoaded: false
  };

  componentDidMount = () => {
    this.fillReduxFromStorage();
  };

  fillReduxFromStorage = () => {
    this.props.verifyIfUserIsLogged();
    this.setState({ reduxLoaded: true });
  };

  render = () =>
    this.state.reduxLoaded ? (
      <ConnectedRouter history={history}>
        <Switch>
          <PrivateRoute
            exact
            path="/oportunidades/minhas-oportunidades"
            component={OpportunityVoluntaryList}
          />
          <PrivateRoute
            exact
            path="/doacoes/gerenciador/lista"
            component={DonationManagement}
          />
          <PrivateRoute
            exact
            path="/oportunidades/gerenciador/cadastro"
            component={OpportunityRegister}
          />
          <PrivateRoute
            exact
            path="/oportunidades/minhas-oportunidades/detalhes"
            component={OpportunityVoluntaryDetail}
          />
          <PrivateRoute
            exact
            path="/oportunidades/gerenciador/candidatos"
            component={OpportunityCandidates}
          />
          <PrivateRoute
            exact
            path="/oportunidades/gerenciador/lista"
            component={OpportunityManagement}
          />
          <PrivateRoute
            exact
            path="/doacoes/gerenciador/cadastro"
            component={DonationRegister}
          />
          <PrivateRoute exact path="/meus-dados" component={Register} />
          <PrivateRoute
            exact
            path="/minhas-oportunidades"
            component={Opportunity}
          />
          <PrivateRoute exact path="/minhas-doacoes" component={Donation} />
          <Route exact path="/" component={Home} />
          <Route exact path="/cadastro" component={Register} />
          <Route exact path="/contato" component={Contact} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/voluntarios" component={VoluntaryList} />
          <Route
            exact
            path="/voluntarios/detalhes"
            component={VoluntaryDetail}
          />
          <Route exact path="/doacoes/detalhes" component={DonationDetail} />
          <Route exact path="/oportunidades" component={OpportunityList} />
          <Route
            exact
            path="/oportunidades/detalhes"
            component={OpportunityDetail}
          />
          <Route exact path="/doacoes" component={DonationList} />
          <Route
            exact
            path="/esqueci-minha-senha"
            exact
            component={ForgotPassword}
          />
          <Route
            exact
            path="/esqueci-minha-senha/codigo"
            exact
            component={PasswordRecoveryCode}
          />
          <Route
            exact
            path="/esqueci-minha-senha/codigo/:code"
            exact
            component={PasswordRecovery}
          />
        </Switch>
      </ConnectedRouter>
    ) : null;
}

const mapStateToProps = ({ auth, app }) => {
  const { loggedUser } = auth;
  return {
    loggedUser,
    ...app
  };
};

export default connect(
  mapStateToProps,
  {
    verifyIfUserIsLogged,
    resetUserRegisterForm,
    fillUserRegisterFromLoggedUser
  }
)(Routes);
