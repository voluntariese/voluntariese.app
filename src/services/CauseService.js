import WebApi from './WebApi';

export class CauseService {
  static async search() {
    const response = await WebApi.get('/causas');
    return response.data;
  }
}
