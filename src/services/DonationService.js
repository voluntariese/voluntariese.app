import WebApi from './WebApi';

export class DonationService {
  static async search(idCausa) {
    const response = await WebApi.get('/doacoes', { idCausa });
    return response.data;
  }

  static async searchByUser() {
    const response = await WebApi.get('/doacoes/instituicao');
    return response.data;
  }

  static async save(donation) {
    const response = await WebApi.post('/doacoes', {
      descricao: donation.descricao
    });
    return response.data;
  }

  static async update(donation) {
    const response = await WebApi.put(`/doacoes/${donation.id}`, {
      descricao: donation.descricao,
      ativa: donation.ativa
    });
    return response.data;
  }
}
