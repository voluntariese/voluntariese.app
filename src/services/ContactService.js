import WebApi from './WebApi';

export class ContactService {

  static async send(contact) {
    const response = await WebApi.post('/contatos', contact);
    return response.data;
  }
}  