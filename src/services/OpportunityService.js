import WebApi from './WebApi';

export class OpportunityService {
  static async search(idCausa) {
    const response = await WebApi.get('/oportunidades', { idCausa });
    return response.data;
  }

  static async searchByUser() {
    const response = await WebApi.get('/oportunidades/instituicao');
    return response.data;
  }

  static async searchByVoluntary() {
    const response = await WebApi.get('/oportunidades/voluntario');
    return response.data;
  }

  static async candidated(opportunity) {
    const response = await WebApi.post(
      `/oportunidades/${opportunity.id}/candidatura`
    );
    return response.data;
  }

  static async save(opportunity) {
    const response = await WebApi.post('/oportunidades', {
      descricao: opportunity.descricao,
      qualificacoes: opportunity.qualificacoes,
      causa: { id: opportunity.causa },
      turno: opportunity.turno,
      quantidadeVagas: opportunity.quantidadeVagas
    });
    return response.data;
  }

  static async update(opportunity) {
    const response = await WebApi.put(`/oportunidades/${opportunity.id}`, {
      descricao: opportunity.descricao,
      qualificacoes: opportunity.qualificacoes,
      causa: { id: opportunity.causa },
      turno: opportunity.turno,
      quantidadeVagas: opportunity.quantidadeVagas,
      ativa: opportunity.ativa
    });
    return response.data;
  }

  static async approve(opportunity) {
    const response = await WebApi.post(
      `/oportunidades/${opportunity.id}/aprovacao`
    );
    return response.data;
  }

  static async reprove(opportunity, justification) {
    const response = await WebApi.post(
      `/oportunidades/${opportunity.id}/reprovacao`,
      {
        justificativa: justification
      }
    );
    return response.data;
  }
}
