import WebApi from './WebApi';

export class VoluntaryService {
  static async search(idCausa) {
    const response = await WebApi.get('/usuarios/voluntarios', { idCausa });
    return response.data;
  }
}
