export const Messages = {
  LoginExpiredMessage:
    'O seu login expirou. Por favor, informe novamente as suas credenciais.',
  InternalServerError:
    'Não conseguimos conectá-lo. Verifique a sua conexão com a internet.',
  PasswordRecoveryEmailSentMessage:
    'Verifique o seu e-mail para obter o código de recuperação de senha.',
  PasswordRecoveryCodeSuccessValidatedMessage:
    'Informe a sua nova senha e a confirmação para redefinir sua credencial.',
  PasswordSuccessRecoveredMessage:
    'Senha redefinida com sucesso. Utilize a nova senha para realizar o login.',

  UserSuccessRegisteredMessage: 'Cadastro realizado com sucesso!',
  ContactSuccessSentMessage:
    'A sua mensagem foi enviada com sucesso. Em breve, retornaremos o seu contato. Muito obrigado!',
  RequiredEmailValidationMessage: '^Informe o seu e-mail.',
  RequiredPasswordValidationMessage: '^Informe a sua senha.',
  RequiredPasswordRecoveryCodeValidationMessage:
    '^Informe o código de recuperação de senha enviado para seu e-mail.',
  NumericPasswordRecoveryCodeValidationMessage:
    '^O código de recuperação de senha deve conter apenas números.',
  RequiredPhoneValidationMessage: '^Informe o seu telefone.',
  RequiredMessageValidationMessage:
    '^Informe a sua mensagem. Nós queremos te ouvir!',
  RequiredNameValidationMessage: '^Informe o seu nome.',
  InvalidEmailValidationMessage: '^Informe um e-mail válido.',
  RequiredPasswordConfirmationValidationMessage:
    '^Informe a confirmação de senha.',
  InvalidConfirmationPasswordValidationMessage: '^As senhas não conferem.',
  InvalidCpfValidationMessage: '^Informe um CPF válido.',

  RequiredCityValidationMessage: '^Informe a sua cidade.',
  RequiredStateValidationMessage: '^Informe o seu estado.',
  RequiredAddressValidationMessage: '^Informe o seu endereço.',
  RequiredCepValidationMessage: '^Informe o seu CEP.',
  RequiredCpfValidationMessage: '^Informe o seu CPF.',

  UserProfileUpdatedSuccessMessage:
    'Os seus dados foram atualizados com sucesso!',

  RequiredNumberValidationMessage: '^Informe o número da sua residência.',
  RequiredCellPhoneValidationMessage: '^Informe o seu telefone.',
  RequiredBirthdayValidationMessage: '^Informe a sua data de nascimento.',
  InvalidBirthdayValidationMessage: '^Informe uma data de nascimento válida.',
  RequiredCauseOfInterestMessage: '^Seleciona a causa de interesse.',

  RequiredReceiverBirthdayValidationMessage:
    '^Informe a data de nascimento do recebedor.',
  RequiredGenreValidationMessage: '^Informe o seu sexo.',
  RequiredNeighborhoodValidationMessage: '^Informe o seu bairro.',
  RequiredBackOfficeSearchCpfValidationMessage:
    '^É necessário informar um CPF para filtrar os pedidos.',
  BackOfficeOrdersNotFound:
    'Não foram encontrados pedidos para o CPF informado.',

  OrderStatusUpdatedSuccessMessage:
    'O status do pedido foi atualizado com sucesso.',
  OrderServiceOrderUpdatedSuccessMessage:
    'A ordem de serviço do pedido foi atualizada com sucesso.',
  RequiredOrderServiceOrderValidationMessage: 'Informe a ordem de serviço.',
  RequiredMotherNameValidationMessage: '^Informe o nome da mãe.',
  RequiredRgValidationMessage: '^Informe o número do RG.',
  RequiredDescriptionValidationMessage: '^Informe a descrição.',
  DonationSuccessRegisteredMessage: 'Doação salva com sucesso!',
  DonationSuccessUpdatedMessage: 'Doação atualizada com sucesso!',
  VoluntarySuccessCandidatedMessage:
    'Candidatura realizada com sucesso! Aguarde o contato da instituição.',
  RequiredTurnoValidationMessage: '^Informe o turno.',
  RequiredCausaValidationMessage: '^Informe a causa.',
  RequiredQuantidadeVagasValidationMessage: '^Informe a quantidade de vagas.',
  OpportunitySuccessRegisteredMessage: 'Oportunidade salva com sucesso!',
  OpportunitySuccessUpdatedMessage: 'Oportunidade atualizada com sucesso!',
  ApproveOpportunitySuccessdMessage:
    'Candidatura aprovada com sucesso! Entre em contato com o valuntário!',
  ReprovedOpportunitySuccessdMessage: 'Candidatura reprovada com sucesso.'
};
