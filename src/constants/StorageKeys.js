export const StorageKeys = {
  AuthToken: 'Voluntariase_AuthToken',
  AuthTokenExpiresIn: 'Voluntariase_AuthTokenExpiresIn',
  LoggedUser: 'Voluntariase_LoggedUser',
  AuthEmail: 'Voluntariase_AuthEmail',
  Cache: 'Voluntariase_Cache'
};
