import React, { Component } from 'react';
import { Header, Footer, NavigationBar } from '../../components';
import * as VoluntaryDetailActions from '../../actions/VoluntaryDetailActions';
import { connect } from 'react-redux';
import moment from 'moment';
import './style.scss';

class VoluntaryDetail extends Component {
  componentWillMount = () => {
    if (!this.props.voluntaryDetail.nome) this.props.redirectVoluntaryList();
  };

  render = () => (
    <div className="voluntary-detail">
      <Header />
      <NavigationBar
        title="Voluntário"
        breadcrumb={[
          { to: '/', title: 'Home' },
          { to: '/voluntarios', title: 'Voluntários' },
          { to: '/voluntarios/detalhes', title: 'Detalhes' }
        ]}
      />
      <div className="gray-container">
        <div className="product-header">
          <div className="container">
            {this.props.voluntaryDetail.idFotoPerfil && (
              <img
                src={`${process.env.REACT_APP_API_URL}/arquivos/${
                  this.props.voluntaryDetail.idFotoPerfil
                }`}
                className="product-img"
              />
            )}
            <div className="product-info pull-left">
              <h1 className="title">{this.props.voluntaryDetail.nome}</h1>
              <p className="text price">
                Interessado em ajudar{' '}
                {this.props.voluntaryDetail.causasInteresse[0].descricao}
              </p>
            </div>
          </div>
        </div>
        <div className="container details">
          <div className="title-container">
            <i className="fas fa-heart heart-color" />
            <h2 className="title">Informações do Voluntário</h2>
          </div>
          <div className="content">
            <div className="col-xs-12 col-lg-6">
              <h3 className="title secondary">Data de Nascimento</h3>
              <p className="text">
                {moment(this.props.voluntaryDetail.dataNascimento).format(
                  'DD/MM/YYYY'
                )}
              </p>
            </div>
            <div className="col-xs-12 col-lg-6">
              <h3 className="title secondary">Sexo</h3>
              <p className="text">{this.props.voluntaryDetail.sexo}</p>
            </div>
            <div className="col-xs-12 col-lg-6">
              <h3 className="title secondary">Qualificações</h3>
              <p className="text">{this.props.voluntaryDetail.qualificacoes}</p>
            </div>
            <div className="col-xs-12 col-lg-6">
              <h3 className="title secondary">Telefone</h3>
              <p className="text">{this.props.voluntaryDetail.telefone}</p>
            </div>
            <div className="col-xs-12 col-lg-6">
              <h3 className="title secondary">E-mail</h3>
              <p className="text">{this.props.voluntaryDetail.email}</p>
            </div>
            <div className="col-xs-12 col-lg-6">
              <h3 className="title secondary">Localização</h3>
              <p className="text">
                {`${this.props.voluntaryDetail.endereco.cidade}/${
                  this.props.voluntaryDetail.endereco.estado
                }`}
              </p>
            </div>
          </div>
        </div>
      </div>

      <Footer />
    </div>
  );
}

const mapStateToProps = ({ voluntaryDetailReducer }) => {
  return {
    ...voluntaryDetailReducer
  };
};

export default connect(
  mapStateToProps,
  { ...VoluntaryDetailActions }
)(VoluntaryDetail);
