import React, { Component } from 'react';
import moment from 'moment';
import { Header, Footer, NavigationBar } from '../../components';
import * as DonationManagementActions from '../../actions/DonationManagementActions';
import { connect } from 'react-redux';
import './style.scss';

class DonationManagement extends Component {
  componentDidMount = () => {
    this.props.searchManagementDonations();
  };

  redirectToDonationEdit = donation => {
    this.props.redirectToDonationEdit(donation);
  };

  redirectToAddDonation = () => {
    this.props.redirectToAddDonation();
  };

  render = () => (
    <div className="donation-management-list">
      <Header />
      <NavigationBar
        title="Gerenciar Solicitações de Doações"
        breadcrumb={[
          { to: '/', title: 'Home' },
          {
            to: '/doacoes/gerenciador/lista',
            title: 'Gerenciar Solicitações de Doações'
          }
        ]}
      />
      <div className="gray-container">
        <div className="container">
          <div className="row">
            <div
              className="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group text-right"
              title="Cadastrar"
            >
              <button
                className="btn btn-export"
                onClick={() => this.redirectToAddDonation()}
              >
                <span className="glyphicon glyphicon-plus" />
              </button>
            </div>
          </div>
          <div className="table-responsive">
            <table className="table table-striped">
              <thead>
                <tr>
                  <th className="text">Código</th>
                  <th className="text">Descrição</th>
                  <th className="text">Data Cadastro</th>
                  <th className="text">Ativa</th>
                  <th className="text">Ações</th>
                </tr>
              </thead>
              <tbody>
                {this.props.donations.map(d => (
                  <tr key={d.id}>
                    <td className="text small">{d.id}</td>
                    <td className="text small">{d.descricao}</td>
                    <td className="text small">
                      {moment(d.dataCadastro).format('DD/MM/YYYY HH:mm')}
                    </td>
                    <td className="text small">{d.ativa ? 'Sim' : 'Não'}</td>
                    <td>
                      <button
                        className="btn btn-edit"
                        title="Editar"
                        onClick={() => this.redirectToDonationEdit(d)}
                      >
                        <span className="glyphicon glyphicon-pencil" />
                      </button>
                    </td>
                  </tr>
                ))}
                {this.props.donations.length === 0 && (
                  <tr>
                    <td className="text text-center" colSpan="5">
                      Não foram encontradas doações!
                    </td>
                  </tr>
                )}
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}

const mapStateToProps = ({ donationManagementReducer }) => {
  return {
    ...donationManagementReducer
  };
};

export default connect(
  mapStateToProps,
  { ...DonationManagementActions }
)(DonationManagement);
