import React, { Component } from 'react';
import { Header, Footer, NavigationBar } from '../../components';
import * as OpportunityRegisterActions from '../../actions/OpportunityRegisterActions';
import InputMask from 'react-input-mask';
import { connect } from 'react-redux';
import { ValidationService } from '../../services';
import validation from './validation';
import './style.scss';

class OpportunityRegister extends Component {
  componentDidMount = () => {
    this.props.fillCauses();
  };

  formIsValid = () =>
    ValidationService.validate(this.props.opportunity, validation);

  handleChange = (prop, event) => {
    const { value } = event.target;
    this.props.updateOpportunityRegisterField({ prop, value });
  };

  handleSubmit = event => {
    event.preventDefault();
    if (!this.formIsValid()) return;
    if (!this.props.opportunity.id)
      this.props.registerOpportunity(this.props.opportunity);
    else this.props.updateOpportunity(this.props.opportunity);
  };

  render = () => (
    <div className="register-opportunity">
      <Header />
      <NavigationBar
        title={
          this.props.opportunity.id
            ? 'Edição de Oportunidade'
            : 'Cadastro de Oportunidade'
        }
        breadcrumb={[
          { to: '/', title: 'Home' },
          {
            to: '/oportunidades/gerenciador/lista',
            title: 'Gerenciar Oportunidades'
          },
          {
            to: '/oportunidades/gerenciador/cadastro',
            title: 'Cadastro Oportunidades'
          }
        ]}
      />
      <div className="gray-container">
        <div className="container">
          {!this.props.opportunity.id && (
            <p className="text text-center page-description">
              Preencha o formulário abaixo para realizar o cadastro da
              oportunidade.
            </p>
          )}
          {this.props.opportunity.id && (
            <p className="text text-center page-description">
              Preencha o formulário para atualizar a oportunidade.
            </p>
          )}
          <div className="form-container">
            <form onSubmit={this.handleSubmit}>
              <div className="row">
                <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                  <label className="text primary">Descrição *</label>
                  <textarea
                    maxLength="500"
                    name="description"
                    value={this.props.opportunity.descricao}
                    onChange={e => this.handleChange('descricao', e)}
                  />
                </div>
                <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                  <label className="text primary">Qualificações</label>
                  <textarea
                    maxLength="500"
                    name="qualification"
                    value={this.props.opportunity.qualificacoes}
                    onChange={e => this.handleChange('qualificacoes', e)}
                  />
                </div>
                <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                  <label className="text primary">Causa de Interesse *</label>
                  <select
                    name="causa"
                    value={this.props.opportunity.causa}
                    onChange={ev => this.handleChange('causa', ev)}
                  >
                    <option value="">Selecione</option>
                    {this.props.causes.map(c => (
                      <option key={c.id} value={c.id}>
                        {c.descricao}
                      </option>
                    ))}
                  </select>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12 form-group">
                  <label className="text primary">Turno *</label>
                  <input
                    maxLength="200"
                    type="text"
                    name="turno"
                    value={this.props.opportunity.turno}
                    onChange={e => this.handleChange('turno', e)}
                  />
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12 form-group">
                  <label className="text primary">Quantidade de Vagas *</label>
                  <InputMask
                    type="text"
                    mask="999999"
                    alwaysShowMask={false}
                    maskChar=""
                    name="number"
                    value={this.props.opportunity.quantidadeVagas}
                    onChange={e => this.handleChange('quantidadeVagas', e)}
                  />
                </div>
                {this.props.opportunity.id && (
                  <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                    <label className="text primary">Ativa *</label>
                    <select
                      name="active"
                      onChange={e => this.handleChange('ativa', e)}
                      value={this.props.opportunity.ativa}
                    >
                      <option value="true">Sim</option>
                      <option value="false">Não</option>
                    </select>
                  </div>
                )}
                <div className="col-xs-12 text-center btn-container">
                  <button type="submit" className="btn btn-submit">
                    {this.props.opportunity.id ? 'ATUALIZAR' : 'CADASTRAR'}
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}

const mapStateToProps = ({ registerOpportunityReducer }) => {
  return {
    ...registerOpportunityReducer
  };
};

export default connect(
  mapStateToProps,
  { ...OpportunityRegisterActions }
)(OpportunityRegister);
