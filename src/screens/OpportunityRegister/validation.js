import { Messages } from '../../constants';

const validation = {
  descricao: {
    presence: {
      message: Messages.RequiredDescriptionValidationMessage,
      allowEmpty: false
    }
  },
  turno: {
    presence: {
      message: Messages.RequiredTurnoValidationMessage,
      allowEmpty: false
    }
  },
  causa: {
    presence: {
      message: Messages.RequiredCausaValidationMessage,
      allowEmpty: false
    }
  },
  quantidadeVagas: {
    presence: {
      message: Messages.RequiredQuantidadeVagasValidationMessage,
      allowEmpty: false
    },
    numericality: {
      greaterThan: 0,
      message: Messages.RequiredQuantidadeVagasValidationMessage
    }
  }
};

export default validation;
