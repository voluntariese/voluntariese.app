import React, { Component } from 'react';
import {
  Header,
  Footer,
  NavigationBar,
  OpportunityMenu,
  OpportunityListComponent
} from '../../components';
import * as OportunityListActions from '../../actions/OportunityListActions';
import { connect } from 'react-redux';
import './style.scss';

class OpportunityList extends Component {
  componentDidMount = () => {
    this.props.searchOpportunities();
  };

  render = () => (
    <div className="opportunity-list">
      <Header />
      <NavigationBar
        title="Oportunidades"
        breadcrumb={[
          { to: '/', title: 'Home' },
          { to: '/oportunidades', title: 'Oportunidades' }
        ]}
      />
      <div className="row">
        <div className="col-lg-8 left-col">
          <OpportunityListComponent opportunities={this.props.opportunities} />
        </div>
        <div className="col-lg-4 right-col">
          <OpportunityMenu />
        </div>
      </div>
      <Footer />
    </div>
  );
}

const mapStateToProps = ({ opportunityListReducer }) => {
  return {
    ...opportunityListReducer
  };
};

export default connect(
  mapStateToProps,
  { ...OportunityListActions }
)(OpportunityList);
