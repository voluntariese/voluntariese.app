import React, { Component } from 'react';
import { Header, Footer, NavigationBar } from '../../components';
import * as OpportunityManagementActions from '../../actions/OpportunityManagementActions';
import { connect } from 'react-redux';
import moment from 'moment';
import './style.scss';

class OpportunityCandidates extends Component {
  state = {
    openBox: false
  };

  componentWillMount = () => {
    if (!this.props.opportunityCandidates.instituicao.nome)
      this.props.redirectOpportunityList();
  };

  approve = opportunity => {
    this.props.approve(opportunity);
  };

  reprove = () => {
    this.setState({ openBox: true });
  };

  cancelEdition = () => {
    this.setState({ openBox: false });
  };

  handleReproveSubmit = opportunity => {
    this.props.reproveOpportunity(opportunity, this.props.justification);
  };

  handleChange = event => {
    this.props.updateJustification(event.target.value);
  };

  render = () => (
    <div className="opportunity-candidates">
      <Header />
      <NavigationBar
        title="Gerenciar Candidatos da Oportunidade"
        breadcrumb={[
          { to: '/', title: 'Home' },
          {
            to: '/oportunidades/gerenciador/lista',
            title: 'Gerenciar Oportunidades'
          },
          {
            to: '/oportunidades/gerenciador/candidatos',
            title: 'Oportunidades Candidatos'
          }
        ]}
      />
      <div className="gray-container">
        <div className="product-header">
          <div className="container">
            {this.props.opportunityCandidates.instituicao.idFotoPerfil && (
              <img
                src={`${process.env.REACT_APP_API_URL}/arquivos/${
                  this.props.opportunityCandidates.instituicao.idFotoPerfil
                }`}
                className="product-img"
              />
            )}
            <div className="product-info pull-left">
              <h1 className="title">
                {this.props.opportunityCandidates.instituicao.nome}
              </h1>
              <p className="text price">
                Interessado em ajudar{' '}
                {
                  this.props.opportunityCandidates.instituicao
                    .causasInteresse[0].descricao
                }
              </p>
            </div>
          </div>
        </div>
        <div className="container details">
          <div className="title-container">
            <i className="fas fa-heart heart-color" />
            <h2 className="title">Informações da Oportunidade</h2>
          </div>
          <div className="content">
            <div className="col-xs-12 col-lg-6">
              <h3 className="title secondary">Descrição</h3>
              <p className="text">
                {this.props.opportunityCandidates.descricao}
              </p>
            </div>
            <div className="col-xs-12 col-lg-6">
              <h3 className="title secondary">Data da Divulgação</h3>
              <p className="text">
                {moment(this.props.opportunityCandidates.dataCriacao).format(
                  'DD/MM/YYYY'
                )}
              </p>
            </div>
            <div className="col-xs-12 col-lg-6">
              <h3 className="title secondary">Quantidade de Vagas</h3>
              <p className="text">
                {this.props.opportunityCandidates.quantidadeVagas}
              </p>
            </div>
            <div className="col-xs-12 col-lg-6">
              <h3 className="title secondary">Turno</h3>
              <p className="text">{this.props.opportunityCandidates.turno}</p>
            </div>
            <div className="col-xs-12 col-lg-6">
              <h3 className="title secondary">Qualificações</h3>
              <p className="text">
                {this.props.opportunityCandidates.qualificacoes ||
                  'Não é necessário possuir qualificações'}
              </p>
            </div>
            <div className="col-xs-12 col-lg-6">
              <h3 className="title secondary">Localização</h3>
              <p className="text">
                {`${
                  this.props.opportunityCandidates.instituicao.endereco
                    .logradouro
                }, ${
                  this.props.opportunityCandidates.instituicao.endereco.numero
                } -  ${
                  this.props.opportunityCandidates.instituicao.endereco.bairro
                } - ${
                  this.props.opportunityCandidates.instituicao.endereco.cidade
                }/${
                  this.props.opportunityCandidates.instituicao.endereco.estado
                }`}
              </p>
            </div>
            <div className="col-xs-12 col-lg-6">
              <h3 className="title secondary">Telefone</h3>
              <p className="text">
                {this.props.opportunityCandidates.instituicao.telefone}
              </p>
            </div>
            <div className="col-xs-12 col-lg-6">
              <h3 className="title secondary">E-mail</h3>
              <p className="text">
                {this.props.opportunityCandidates.instituicao.email}
              </p>
            </div>
          </div>
        </div>
        <div className="container details">
          <div className="title-container">
            <i className="fas fa-heart heart-color" />
            <h2 className="title">Informações dos Candidatos</h2>
          </div>
          <div className="content">
            {this.props.opportunityCandidates.interessados.length > 0 && (
              <div className="table-responsive">
                <table className="table table-striped">
                  <thead>
                    <tr>
                      <th className="text">Voluntário</th>
                      <th className="text">Telefone</th>
                      <th className="text">Data Candidatura</th>
                      <th className="text">Status</th>
                      <th className="text">Justificativa</th>
                      <th className="text">Ações</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.props.opportunityCandidates.interessados.map(i => (
                      <tr key={i.id}>
                        <td className="text small voluntary">
                          <a href="/voluntarios" target="_blank">
                            {i.voluntario.nome}
                          </a>
                        </td>
                        <td className="text small">{i.voluntario.telefone}</td>
                        <td className="text small">
                          {moment(i.dataCriacao).format('DD/MM/YYYY HH:mm')}
                        </td>
                        <td className="text small">
                          {i.aprovada
                            ? 'Aprovada'
                            : i.justificativa
                            ? 'Reprovada'
                            : 'Aguardando Análise'}
                        </td>
                        <td className="text small">{i.justificativa}</td>
                        <td
                          className={`text small ${
                            this.state.openBox ? 'hidden' : 'visible'
                          }`}
                        >
                          {!i.aprovada && !i.justificativa && (
                            <button
                              className="btn btn-edit"
                              title="Aprovar"
                              onClick={() => this.approve(i)}
                            >
                              <span className="glyphicon glyphicon-ok" />
                            </button>
                          )}
                          {!i.aprovada && !i.justificativa && (
                            <button
                              className="btn btn-edit remove"
                              title="Reprovar"
                              onClick={() => this.reprove(i)}
                            >
                              <span className="glyphicon glyphicon-remove" />
                            </button>
                          )}
                        </td>
                        <td
                          colSpan="2"
                          className={`text small ${
                            this.state.openBox ? 'visible' : 'hidden'
                          }`}
                        >
                          <form className="form-inline form-edit">
                            <div className="form-group">
                              <label>Justificativa</label>
                              <textarea
                                value={this.props.justification}
                                onChange={e => this.handleChange(e)}
                              />
                            </div>
                            <button
                              type="button"
                              className="btn"
                              title="Salvar"
                              onClick={e => this.handleReproveSubmit(i)}
                            >
                              <span className="glyphicon glyphicon-floppy-disk" />
                            </button>
                            <button
                              type="button"
                              className="btn btn-remove"
                              title="Cancelar"
                              onClick={this.cancelEdition}
                            >
                              <span className="glyphicon glyphicon-remove" />
                            </button>
                          </form>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            )}
            {this.props.opportunityCandidates.interessados.length <= 0 && (
              <p className="text not-found">
                Nenhum interessado foi encontrado
              </p>
            )}
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}

const mapStateToProps = ({ opportunityManagementReducer }) => {
  return {
    ...opportunityManagementReducer
  };
};

export default connect(
  mapStateToProps,
  { ...OpportunityManagementActions }
)(OpportunityCandidates);
