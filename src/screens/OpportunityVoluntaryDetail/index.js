import React, { Component } from 'react';
import { Header, Footer, NavigationBar } from '../../components';
import * as OpportunityVoluntaryActions from '../../actions/OpportunityVoluntaryActions';
import { connect } from 'react-redux';
import moment from 'moment';
import './style.scss';

class OpportunityVoluntaryDetail extends Component {
  componentWillMount = () => {
    if (!this.props.opportunityDetail.instituicao.nome)
      this.props.redirectToVoluntaryList();
  };

  render = () => {
    const detail =
      this.props.opportunityDetail.interessados.find(
        i => i.voluntario.id === this.props.loggedUser.id
      ) || {};
    return (
      <div className="opportunity-voluntary-detail">
        <Header />
        <NavigationBar
          title="Detalhes de Oportunidade"
          breadcrumb={[
            { to: '/', title: 'Home' },
            {
              to: '/oportunidades/minhas-oportunidades',
              title: 'Minhas Oportunidades'
            },
            {
              to: '/oportunidades/minhas-oportunidades/detalhes',
              title: 'Detalhes'
            }
          ]}
        />
        <div className="gray-container">
          <div className="product-header">
            <div className="container">
              {this.props.opportunityDetail.instituicao.idFotoPerfil && (
                <img
                  src={`${process.env.REACT_APP_API_URL}/arquivos/${
                    this.props.opportunityDetail.instituicao.idFotoPerfil
                  }`}
                  className="product-img"
                />
              )}
              <div className="product-info pull-left">
                <h1 className="title">
                  {this.props.opportunityDetail.instituicao.nome}
                </h1>
                <p className="text price">
                  Interessado em ajudar{' '}
                  {
                    this.props.opportunityDetail.instituicao.causasInteresse[0]
                      .descricao
                  }
                </p>
              </div>
            </div>
          </div>
          <div className="container details">
            <div className="title-container">
              <i className="fas fa-heart heart-color" />
              <h2 className="title">Informações da Oportunidade</h2>
            </div>
            <div className="content">
              <div className="col-xs-12 col-lg-6">
                <h3 className="title secondary">Status</h3>
                <p className="text">
                  {detail.aprovada
                    ? 'Aprovada'
                    : detail.justificativa
                    ? 'Reprovada'
                    : 'Aguardando Análise'}
                </p>
              </div>
              <div className="col-xs-12 col-lg-6">
                <h3 className="title secondary">Data da Candidatura</h3>
                <p className="text">
                  {moment(detail.dataCriacao).format('DD/MM/YYYY')}
                </p>
              </div>
              {detail.justificativa && (
                <div className="col-xs-12 col-lg-12">
                  <h3 className="title secondary">Justificativa</h3>
                  <p className="text">{detail.justificativa}</p>
                </div>
              )}
              <div className="col-xs-12 col-lg-6">
                <h3 className="title secondary">Descrição</h3>
                <p className="text">{this.props.opportunityDetail.descricao}</p>
              </div>
              <div className="col-xs-12 col-lg-6">
                <h3 className="title secondary">Data da Divulgação</h3>
                <p className="text">
                  {moment(this.props.opportunityDetail.dataCriacao).format(
                    'DD/MM/YYYY'
                  )}
                </p>
              </div>
              <div className="col-xs-12 col-lg-6">
                <h3 className="title secondary">Quantidade de Vagas</h3>
                <p className="text">
                  {this.props.opportunityDetail.quantidadeVagas}
                </p>
              </div>
              <div className="col-xs-12 col-lg-6">
                <h3 className="title secondary">Turno</h3>
                <p className="text">{this.props.opportunityDetail.turno}</p>
              </div>
              <div className="col-xs-12 col-lg-6">
                <h3 className="title secondary">Qualificações</h3>
                <p className="text">
                  {this.props.opportunityDetail.qualificacoes ||
                    'Não é necessário possuir qualificações'}
                </p>
              </div>
              <div className="col-xs-12 col-lg-6">
                <h3 className="title secondary">Localização</h3>
                <p className="text">
                  {`${
                    this.props.opportunityDetail.instituicao.endereco.logradouro
                  }, ${
                    this.props.opportunityDetail.instituicao.endereco.numero
                  } -  ${
                    this.props.opportunityDetail.instituicao.endereco.bairro
                  } - ${
                    this.props.opportunityDetail.instituicao.endereco.cidade
                  }/${
                    this.props.opportunityDetail.instituicao.endereco.estado
                  }`}
                </p>
              </div>
              <div className="col-xs-12 col-lg-6">
                <h3 className="title secondary">Telefone</h3>
                <p className="text">
                  {this.props.opportunityDetail.instituicao.telefone}
                </p>
              </div>
              <div className="col-xs-12 col-lg-6">
                <h3 className="title secondary">E-mail</h3>
                <p className="text">
                  {this.props.opportunityDetail.instituicao.email}
                </p>
              </div>
            </div>
          </div>
        </div>

        <Footer />
      </div>
    );
  };
}

const mapStateToProps = ({ opportunityVoluntaryReducer, auth }) => {
  return {
    ...opportunityVoluntaryReducer,
    ...auth
  };
};

export default connect(
  mapStateToProps,
  { ...OpportunityVoluntaryActions }
)(OpportunityVoluntaryDetail);
