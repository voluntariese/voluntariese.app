import React, { Component } from 'react';
import { Header, Footer, NavigationBar } from '../../components';
import * as DonationManagementActions from '../../actions/DonationManagementActions';
import { connect } from 'react-redux';
import { ValidationService } from '../../services';
import validation from './validation';
import './style.scss';

class DonationRegister extends Component {
  formIsValid = () =>
    ValidationService.validate(this.props.donation, validation);

  handleChange = (prop, event) => {
    const { value } = event.target;
    this.props.updateDonationRegisterField({ prop, value });
  };

  handleSubmit = event => {
    event.preventDefault();
    if (!this.formIsValid()) return;
    if (!this.props.donation.id)
      this.props.registerDonation(this.props.donation);
    else this.props.updateDonation(this.props.donation);
  };

  render = () => (
    <div className="register-donation">
      <Header />
      <NavigationBar
        title={
          this.props.donation.id
            ? 'Edição Solicitação de Doação'
            : 'Cadastro Solicitação de Doação'
        }
        breadcrumb={[
          { to: '/', title: 'Home' },
          { to: '/doacoes/gerenciador/lista', title: 'Gerenciar Doações' },
          {
            to: '/doacoes/gerenciador/cadastro',
            title: 'Cadastro Solicitação Doação'
          }
        ]}
      />
      <div className="gray-container">
        <div className="container">
          {!this.props.donation.id && (
            <p className="text text-center page-description">
              Informe a descrição da doação para realizar o cadastro.
            </p>
          )}
          {this.props.donation.id && (
            <p className="text text-center page-description">
              Preencha o formulário para atualizar a doação.
            </p>
          )}
          <div className="form-container">
            <form onSubmit={this.handleSubmit}>
              <div className="row">
                <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                  <label className="text primary">Descrição *</label>
                  <textarea
                    maxLength="500"
                    name="description"
                    value={this.props.donation.descricao}
                    onChange={e => this.handleChange('descricao', e)}
                  />
                </div>
                {this.props.donation.id && (
                  <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                    <label className="text primary">Ativa *</label>
                    <select
                      name="active"
                      onChange={e => this.handleChange('ativa', e)}
                      value={this.props.donation.ativa}
                    >
                      <option value="true">Sim</option>
                      <option value="false">Não</option>
                    </select>
                  </div>
                )}
                <div className="col-xs-12 text-center btn-container">
                  <button type="submit" className="btn btn-submit">
                    {this.props.donation.id ? 'ATUALIZAR' : 'CADASTRAR'}
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}

const mapStateToProps = ({ donationManagementReducer }) => {
  return {
    ...donationManagementReducer
  };
};

export default connect(
  mapStateToProps,
  { ...DonationManagementActions }
)(DonationRegister);
