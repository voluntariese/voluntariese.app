import { Messages } from '../../constants';

const validation = {
  descricao: {
    presence: {
      message: Messages.RequiredDescriptionValidationMessage,
      allowEmpty: false
    }
  }
};

export default validation;
