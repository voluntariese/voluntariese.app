import React, { Component } from 'react';
import {
  Header,
  Footer,
  NavigationBar,
  VoluntaryListComponent,
  VoluntaryMenu
} from '../../components';
import * as VoluntaryListActions from '../../actions/VoluntaryListActions';
import { connect } from 'react-redux';
import './style.scss';

class VoluntaryList extends Component {
  componentDidMount = () => {
    this.props.searchVoluntaries();
  };

  render = () => (
    <div className="voluntary-list">
      <Header />
      <NavigationBar
        title="Voluntários"
        breadcrumb={[
          { to: '/', title: 'Home' },
          { to: '/voluntarios', title: 'Voluntários' }
        ]}
      />
      <div className="row">
        <div className="col-lg-8 left-col">
          <VoluntaryListComponent voluntaries={this.props.voluntaries} />
        </div>
        <div className="col-lg-4 right-col">
          <VoluntaryMenu />
        </div>
      </div>
      <Footer />
    </div>
  );
}

const mapStateToProps = ({ voluntaryListReducer }) => {
  return {
    ...voluntaryListReducer
  };
};

export default connect(
  mapStateToProps,
  { ...VoluntaryListActions }
)(VoluntaryList);
