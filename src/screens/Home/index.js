import React, { Component } from 'react';
import { Header, Footer, NavigationBar } from '../../components';
import './style.scss';

class Home extends Component {
  render = () => (
    <div className="home">
      <Header />
      <div className="gray-container text-center">
        <span className="text title">Voluntarie-se</span>
        <p className="text">
          Encontre e publique oportunidades para fazer um mundo melhor!
        </p>
      </div>
      <div className="row citations">
        <div className="col-lg-4 text-center">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
          orci nisl, pharetra sed enim ac, egestas efficitur lacus. Aenean et
          libero massa. Proin sollicitudin, purus eget fermentum pellentesque,
          mi magna imperdiet elit, id tristique est dui id arcu. Etiam semper
          nibh ut velit luctus laoreet. Praesent sed nulla fringilla, cursus
          turpis a, condimentum nunc. Fusce vehicula pharetra accumsan. Fusce
          rhoncus sem et magna congue, nec dignissim ligula bibendum. Etiam
          libero velit, egestas eget porta a, aliquam eu metus.
        </div>
        <div className="col-lg-4 text-center">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
          orci nisl, pharetra sed enim ac, egestas efficitur lacus. Aenean et
          libero massa. Proin sollicitudin, purus eget fermentum pellentesque,
          mi magna imperdiet elit, id tristique est dui id arcu. Etiam semper
          nibh ut velit luctus laoreet. Praesent sed nulla fringilla, cursus
          turpis a, condimentum nunc. Fusce vehicula pharetra accumsan. Fusce
          rhoncus sem et magna congue, nec dignissim ligula bibendum. Etiam
          libero velit, egestas eget porta a, aliquam eu metus.
        </div>
        <div className="col-lg-4 text-center">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
          orci nisl, pharetra sed enim ac, egestas efficitur lacus. Aenean et
          libero massa. Proin sollicitudin, purus eget fermentum pellentesque,
          mi magna imperdiet elit, id tristique est dui id arcu. Etiam semper
          nibh ut velit luctus laoreet. Praesent sed nulla fringilla, cursus
          turpis a, condimentum nunc. Fusce vehicula pharetra accumsan. Fusce
          rhoncus sem et magna congue, nec dignissim ligula bibendum. Etiam
          libero velit, egestas eget porta a, aliquam eu metus.
        </div>
      </div>
      <Footer />
    </div>
  );
}

export default Home;
