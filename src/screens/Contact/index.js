import React, { Component } from 'react';
import { connect } from 'react-redux';
import InputMask from 'react-input-mask';
import { 
  Header, 
  Footer,
  NavigationBar
} from '../../components';
import { sendContact, updateContactField } from '../../actions/ContactActions';
import './style.scss';
import { ValidationService } from '../../services';
import validation from './validation';

class Contact extends Component {

  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  formIsValid = () => ValidationService.validate(this.props, validation);

  handleChange = (prop, event) => {
    const { value } = event.target;
    this.props.updateContactField({ prop, value });
  };

  handleSubmit = (event) => {
    event.preventDefault();
    if (!this.formIsValid()) return;
    this.props.sendContact(this.props);
  };

  render = () => (
    <div className="contact">
      <Header />
      <NavigationBar title="Contato" breadcrumb={[
        { to: '/', title: 'Home' },
        { to: '/contato', title: 'Contato' },
      ]}/>
      <div className="gray-container">
        <div className="container">
          <p className="text text-center page-description">Queremos muito escutar sua opinião! Deixe seu comentário, dúvidas ou sugestões.</p>
          <div className="row">
            <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-container">
              <h2 className="title text-center semi-bold">Formulário de Contato</h2>
              <form onSubmit={this.handleSubmit}>
                <div className="input-group">
                  <label className="text">Nome *</label>
                  <input type="text" name="name" autoComplete="off" value={this.props.name} onChange={(e) => this.handleChange('name', e)}/>
                </div>
                <div className="input-group">
                  <label className="text">E-mail *</label>
                  <input placeholder="seuemail@email.com" type="text" name="email" autoComplete="off" value={this.props.email} onChange={(e) => this.handleChange('email', e)}/>
                </div>
                <div className="input-group">
                  <label className="text">Telefone *</label>
                  <InputMask type="text" placeholder="(xx) 99999-9999" mask="(99) 99999-9999" alwaysShowMask={false} maskChar="" name="phone" value={this.props.phone} onChange={e => this.handleChange('phone', e)}/>
                </div>
                <div className="input-group">
                  <label className="text">Mensagem *</label>
                  <textarea rows="5" name="message" autoComplete="off" onChange={(e) => this.handleChange('message', e)} value={this.props.message} />
                </div>
                <div className="btn-container text-center">
                  <button className="btn btn-submit" type="submit">ENVIAR</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
  
}

const mapStateToProps = ({ contact }) => {
  return { 
    ...contact
  };
};

export default connect(
  mapStateToProps,
  { sendContact, updateContactField }
)(Contact);
