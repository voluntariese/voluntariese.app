import React, { Component } from 'react';
import moment from 'moment';
import { Header, Footer, NavigationBar } from '../../components';
import * as OpportunityVoluntaryActions from '../../actions/OpportunityVoluntaryActions';
import { connect } from 'react-redux';
import './style.scss';

class OpportunityVoluntaryList extends Component {
  componentDidMount = () => {
    this.props.searchVoluntaryOpportunities();
  };

  redirectToOpportunityDetail = opportunity => {
    this.props.redirectToOpportunityDetail(opportunity);
  };

  render = () => (
    <div className="opportunity-voluntary-list">
      <Header />
      <NavigationBar
        title="Minhas Oportunidades"
        breadcrumb={[
          { to: '/', title: 'Home' },
          {
            to: '/oportunidades/minhas-oportunidades',
            title: 'Minhas Oportunidades'
          }
        ]}
      />
      <div className="gray-container">
        <div className="container">
          <div className="table-responsive">
            <table className="table table-striped">
              <thead>
                <tr>
                  <th className="text">Código</th>
                  <th className="text">Data Candidatura</th>
                  <th className="text">Instituição</th>
                  <th className="text">Descrição</th>
                  <th className="text">Status</th>
                  <th className="text">Detalhes</th>
                </tr>
              </thead>
              <tbody>
                {this.props.opportunities.map(o => (
                  <tr key={o.id}>
                    <td className="text small">{o.id}</td>
                    <td className="text small">
                      {moment(
                        o.interessados.find(
                          i => i.voluntario.id === this.props.loggedUser.id
                        ).dataCriacao
                      ).format('DD/MM/YYYY HH:mm')}
                    </td>
                    <td className="text small">{o.instituicao.nome}</td>
                    <td className="text small">{o.descricao}</td>
                    <td className="text small">
                      {o.interessados.find(
                        i => i.voluntario.id === this.props.loggedUser.id
                      ).aprovada
                        ? 'Aprovada'
                        : o.interessados.find(
                            i => i.voluntario.id === this.props.loggedUser.id
                          ).justificativa
                        ? 'Reprovada'
                        : 'Aguardando Análise'}
                    </td>
                    <td>
                      <button
                        className="btn btn-edit"
                        title="Detalhes"
                        onClick={() => this.redirectToOpportunityDetail(o)}
                      >
                        <span className="glyphicon glyphicon-eye-open" />
                      </button>
                    </td>
                  </tr>
                ))}
                {this.props.opportunities.length === 0 && (
                  <tr>
                    <td className="text text-center" colSpan="7">
                      Não foram encontradas oportunidades!
                    </td>
                  </tr>
                )}
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}

const mapStateToProps = ({ opportunityVoluntaryReducer, auth }) => {
  const { loggedUser } = auth;
  return {
    ...opportunityVoluntaryReducer,
    loggedUser
  };
};

export default connect(
  mapStateToProps,
  { ...OpportunityVoluntaryActions }
)(OpportunityVoluntaryList);
