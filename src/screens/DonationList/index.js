import React, { Component } from 'react';
import {
  Header,
  Footer,
  NavigationBar,
  DonateListComponent,
  DonateMenu
} from '../../components';
import * as DonationListActions from '../../actions/DonationListActions';
import { connect } from 'react-redux';
import './style.scss';

class DonationList extends Component {
  componentDidMount = () => {
    this.props.searchDonations();
  };

  render = () => (
    <div className="donation">
      <Header />
      <NavigationBar
        title="Doações"
        breadcrumb={[
          { to: '/', title: 'Home' },
          { to: '/doacoes', title: 'Doações' }
        ]}
      />
      <div className="row">
        <div className="col-lg-8 left-col">
          <DonateListComponent donations={this.props.donations} />
        </div>
        <div className="col-lg-4 right-col">
          <DonateMenu />
        </div>
      </div>
      <Footer />
    </div>
  );
}

const mapStateToProps = ({ donationListReducer }) => {
  return {
    ...donationListReducer
  };
};

export default connect(
  mapStateToProps,
  { ...DonationListActions }
)(DonationList);
