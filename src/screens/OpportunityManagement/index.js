import React, { Component } from 'react';
import moment from 'moment';
import { Header, Footer, NavigationBar } from '../../components';
import * as OpportunityManagementActions from '../../actions/OpportunityManagementActions';
import { connect } from 'react-redux';
import './style.scss';

class OpportunityManagement extends Component {
  componentDidMount = () => {
    this.props.searchManagementOpportunities();
  };

  redirectToEditOpportunity = opportunity => {
    this.props.redirectToEditOpportunity(opportunity);
  };

  redirectToAddOpportunity = () => {
    this.props.redirectToAddOpportunity();
  };

  redirectToCandidates = opportunities => {
    this.props.redirectToCandidates(opportunities);
  };

  render = () => (
    <div className="opportunity-management-list">
      <Header />
      <NavigationBar
        title="Gerenciar Oportunidades"
        breadcrumb={[
          { to: '/', title: 'Home' },
          {
            to: '/oportunidades/gerenciador/lista',
            title: 'Gerenciar Oportunidades'
          }
        ]}
      />
      <div className="gray-container">
        <div className="container">
          <div className="row">
            <div
              className="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group text-right"
              title="Cadastrar"
            >
              <button
                className="btn btn-export"
                onClick={() => this.redirectToAddOpportunity()}
              >
                <span className="glyphicon glyphicon-plus" />
              </button>
            </div>
          </div>
          <div className="table-responsive">
            <table className="table table-striped">
              <thead>
                <tr>
                  <th className="text">Código</th>
                  <th className="text">Descrição</th>
                  <th className="text">Data Cadastro</th>
                  <th className="text">Aprovação Pendente</th>
                  <th className="text">Ativa</th>
                  <th className="text">Ações</th>
                </tr>
              </thead>
              <tbody>
                {this.props.opportunities.map(o => (
                  <tr key={o.id}>
                    <td className="text small">{o.id}</td>
                    <td className="text small">{o.descricao}</td>
                    <td className="text small">
                      {moment(o.dataCadastro).format('DD/MM/YYYY HH:mm')}
                    </td>
                    <td className="text small">
                      {o.interessados.filter(
                        i => !i.aprovada && !i.justificativa
                      ).length > 0
                        ? 'Sim'
                        : 'Não'}
                    </td>
                    <td className="text small">{o.ativa ? 'Sim' : 'Não'}</td>
                    <td>
                      <button
                        className="btn btn-edit"
                        title="Editar"
                        onClick={() => this.redirectToEditOpportunity(o)}
                      >
                        <span className="glyphicon glyphicon-pencil" />
                      </button>
                      <button
                        className="btn btn-edit"
                        title="Candidatos"
                        onClick={() => this.redirectToCandidates(o)}
                      >
                        <span className="glyphicon glyphicon-user" />
                      </button>
                    </td>
                  </tr>
                ))}
                {this.props.opportunities.length === 0 && (
                  <tr>
                    <td className="text text-center" colSpan="8">
                      Não foram encontradas oportunidades!
                    </td>
                  </tr>
                )}
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}

const mapStateToProps = ({ opportunityManagementReducer }) => {
  return {
    ...opportunityManagementReducer
  };
};

export default connect(
  mapStateToProps,
  { ...OpportunityManagementActions }
)(OpportunityManagement);
