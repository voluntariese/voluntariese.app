import React, { Component } from 'react';
import { Header, Footer, NavigationBar } from '../../components';
import * as DonationDetailActions from '../../actions/DonationDetailActions';
import { connect } from 'react-redux';
import './style.scss';

class DonationDetail extends Component {
  componentWillMount = () => {
    if (!this.props.donationDetail.instituicao.nome)
      this.props.redirectDonationList();
  };

  render = () => (
    <div className="donation-detail">
      <Header />
      <NavigationBar
        title="Doação"
        breadcrumb={[
          { to: '/', title: 'Home' },
          { to: '/doacoes', title: 'Doações' },
          { to: '/doacoes/detalhes', title: 'Detalhes' }
        ]}
      />
      <div className="gray-container">
        <div className="product-header">
          <div className="container">
            {this.props.donationDetail.instituicao.idFotoPerfil && (
              <img
                src={`${process.env.REACT_APP_API_URL}/arquivos/${
                  this.props.donationDetail.instituicao.idFotoPerfil
                }`}
                className="product-img"
              />
            )}
            <div className="product-info pull-left">
              <h1 className="title">
                {this.props.donationDetail.instituicao.nome}
              </h1>
              <p className="text price">
                Interessado em ajudar{' '}
                {
                  this.props.donationDetail.instituicao.causasInteresse[0]
                    .descricao
                }
              </p>
            </div>
          </div>
        </div>
        <div className="container details">
          <div className="title-container">
            <i className="fas fa-heart heart-color" />
            <h2 className="title">Informações da Doação</h2>
          </div>
          <div className="content">
            <div className="col-xs-12 col-lg-12">
              <h3 className="title secondary">Descrição</h3>
              <p className="text">{this.props.donationDetail.descricao}</p>
            </div>
            <div className="col-xs-12 col-lg-12">
              <h3 className="title secondary">Localização</h3>
              <p className="text">
                {`${
                  this.props.donationDetail.instituicao.endereco.logradouro
                }, ${
                  this.props.donationDetail.instituicao.endereco.numero
                } -  ${
                  this.props.donationDetail.instituicao.endereco.bairro
                } - ${this.props.donationDetail.instituicao.endereco.cidade}/${
                  this.props.donationDetail.instituicao.endereco.estado
                }`}
              </p>
            </div>
            <div className="col-xs-12 col-lg-6">
              <h3 className="title secondary">Telefone</h3>
              <p className="text">
                {this.props.donationDetail.instituicao.telefone}
              </p>
            </div>
            <div className="col-xs-12 col-lg-6">
              <h3 className="title secondary">E-mail</h3>
              <p className="text">
                {this.props.donationDetail.instituicao.email}
              </p>
            </div>
          </div>
        </div>
      </div>

      <Footer />
    </div>
  );
}

const mapStateToProps = ({ donationDetailReducer }) => {
  return {
    ...donationDetailReducer
  };
};

export default connect(
  mapStateToProps,
  { ...DonationDetailActions }
)(DonationDetail);
