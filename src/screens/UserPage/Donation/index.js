import React, { Component } from 'react';
import { Header, Footer, NavigationBar } from '../../../components';
import './style.scss';

class Donation extends Component {
  render = () => {
    const { colLg, colMd, colSm, colXs } = this.props;
    return (
      <div className="login">
        <Header />
        <NavigationBar
          title="Login"
          breadcrumb={[
            { to: '/', title: 'Home' },
            { to: '/login', title: 'Login' }
          ]}
        />
        <Footer />
      </div>
    );
  };
}

export default Donation;
