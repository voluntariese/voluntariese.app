import React, { Component } from 'react';
import { 
  Header, 
  Footer,
  NavigationBar
} from '../../../components';
import './style.scss';

class Opportunity extends Component {

  render = () => (
    <div className="login">
      <Header />
      <NavigationBar title="Login" breadcrumb={[
        { to: '/', title: 'Home' },
        { to: '/login', title: 'Login' },
      ]}/>
      <div className="gray-container">
        Listagem de oportunidades
      </div>
      <Footer />
    </div>
  );
}

export default Opportunity;